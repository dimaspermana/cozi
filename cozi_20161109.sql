-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 08, 2016 at 09:07 AM
-- Server version: 5.6.33-log
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `situsdem_cozi`
--

-- --------------------------------------------------------

--
-- Table structure for table `ac`
--

--
-- Table structure for table `institusi_foto`
--

CREATE TABLE IF NOT EXISTS `institusi_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_institusi` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `keterangan` text,
  `lokasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--alter column lokasi table institusi_foto

ALTER TABLE `institusi` ADD `lokasi` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `nama` ;

-- add column bengkel_laporan

ALTER TABLE `bengkel_laporan` add `ac_05_2` INT( 11 ) NULL ,
 add `ac_3_10`  INT( 11 ) NULL ,
 add `ac_10` INT( 11 ) NULL ,
 add `ac_cold`  INT( 11 ) NULL;
