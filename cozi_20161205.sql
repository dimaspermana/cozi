-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2016 at 06:54 
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `cozi`
--

-- --------------------------------------------------------

--
-- Table structure for table `propinsi`
--
DROP TABLE `propinsi` ;

CREATE TABLE IF NOT EXISTS `propinsi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `nama` varchar(200) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `center` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `propinsi`
--

INSERT INTO `propinsi` (`id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `nama`, `slug`, `center`) VALUES
(1, '2016-10-27 19:15:57', NULL, NULL, NULL, NULL, NULL, 'NANGROE ACEH DAROESSALAM', 'aceh', '5.549142, 95.328016'),
(2, '2016-10-27 19:15:57', NULL, NULL, NULL, NULL, NULL, 'SUMATERA UTARA', 'sumut', '3.592261, 98.677733'),
(3, '2016-10-27 19:16:15', NULL, NULL, NULL, NULL, NULL, 'SUMATERA BARAT', 'sumbar', '-0.943643, 100.410496'),
(4, '2016-10-27 19:16:15', NULL, NULL, NULL, NULL, NULL, 'RIAU', 'riau', '0.508298, 101.454222'),
(5, '2016-10-27 19:16:30', NULL, NULL, NULL, NULL, NULL, 'KEPULAUAN RIAU', 'kepri', '0.918521, 104.467775'),
(6, '2016-10-27 19:16:30', NULL, NULL, NULL, NULL, NULL, 'JAMBI', 'jambi', '-1.590188, 103.610902'),
(7, '2016-10-27 19:16:45', NULL, NULL, NULL, NULL, NULL, 'BENGKULU', 'bengkulu', '-3.792117, 102.263723'),
(8, '2016-10-27 19:16:45', NULL, NULL, NULL, NULL, NULL, 'SUMATERA SELATAN', 'sumsel', '-2.975197, 104.781847'),
(9, '2016-10-27 19:17:05', NULL, NULL, NULL, NULL, NULL, 'KEPULAUAN BANGKA BELITUNG', 'babel', '-2.133835, 106.118467'),
(10, '2016-10-27 19:17:05', NULL, NULL, NULL, NULL, NULL, 'LAMPUNG', 'lampung', '-5.395767, 105.262498'),
(11, '2016-10-27 19:17:23', NULL, NULL, NULL, NULL, NULL, 'BANTEN', 'banten', '-6.110654, 106.164170'),
(12, '2016-10-27 19:17:23', NULL, NULL, NULL, NULL, NULL, 'JAWA BARAT', 'jabar', '-6.914697, 107.612945'),
(13, '2016-10-27 19:17:40', NULL, NULL, NULL, NULL, NULL, 'DKI JAKARTA', 'jakarta', '-6.209951, 106.832951'),
(14, '2016-10-27 19:17:40', NULL, NULL, NULL, NULL, NULL, 'JAWA TENGAH', 'jateng', '-6.964119, 110.418162'),
(15, '2016-10-27 19:18:06', NULL, NULL, NULL, NULL, NULL, 'DI YOGYAKARTA', 'yogyakarta', '-7.795767, 110.373815'),
(16, '2016-10-27 19:18:06', NULL, NULL, NULL, NULL, NULL, 'JAWA TIMUR', 'jatim', '-7.258591, 112.753913'),
(17, '2016-10-27 19:18:24', NULL, NULL, NULL, NULL, NULL, 'BALI', 'bali', '-8.651301, 115.212600'),
(18, '2016-10-27 19:18:24', NULL, NULL, NULL, NULL, NULL, 'NUSA TENGGARA BARAT', 'ntb', '-8.583800, 116.119518'),
(19, '2016-10-27 19:18:41', NULL, NULL, NULL, NULL, NULL, 'NUSA TENGGARA TIMUR', 'ntt', '-10.177485, 123.605449'),
(20, '2016-10-27 19:18:41', NULL, NULL, NULL, NULL, NULL, 'KALIMANTAN BARAT', 'kalbar', '-0.023379, 109.343910'),
(21, '2016-10-27 19:19:01', NULL, NULL, NULL, NULL, NULL, 'KALIMANTAN SELATAN', 'kalsel', '-3.318421, 114.599534'),
(22, '2016-10-27 19:19:01', NULL, NULL, NULL, NULL, NULL, 'KALIMANTAN TENGAH', 'kalteng', '-2.216049, 113.912136'),
(23, '2016-10-27 19:19:19', NULL, NULL, NULL, NULL, NULL, 'KALIMANTAN TIMUR', 'kaltim', '-0.493913, 117.141774'),
(24, '2016-10-27 19:19:19', NULL, NULL, NULL, NULL, NULL, 'KALIMANTAN UTARA', 'kaltara', '2.770372, 117.422471'),
(25, '2016-10-27 19:19:35', NULL, NULL, NULL, NULL, NULL, 'GORONTALO', 'gorontalo', '0.544287, 123.055127'),
(26, '2016-10-27 19:19:35', NULL, NULL, NULL, NULL, NULL, 'SULAWESI SELATAN', 'sulsel', '-5.147722, 119.437581'),
(27, '2016-10-27 19:19:53', NULL, NULL, NULL, NULL, NULL, 'SULAWESI TENGGARA', 'sultra', '-4.000119, 122.516102'),
(28, '2016-10-27 19:19:53', NULL, NULL, NULL, NULL, NULL, 'SULAWESI TENGAH', 'sulteng', '-0.895582, 119.880490'),
(29, '2016-10-27 19:20:08', NULL, NULL, NULL, NULL, NULL, 'SULAWESI UTARA', 'sulut', '1.473339, 124.845790'),
(30, '2016-10-27 19:20:08', NULL, NULL, NULL, NULL, NULL, 'SULAWESI BARAT', 'sulbar', '-2.704702, 118.942756'),
(31, '2016-10-27 19:20:20', NULL, NULL, NULL, NULL, NULL, 'MALUKU', 'maluku', '-3.655426, 128.192347'),
(32, '2016-10-27 19:20:20', NULL, NULL, NULL, NULL, NULL, 'MALUKU UTARA', 'malut', '0.790556, 127.374635'),
(33, '2016-10-27 19:20:33', NULL, NULL, NULL, NULL, NULL, 'PAPUA', 'papua', '-2.589832, 140.669355'),
(34, '2016-10-27 19:20:33', NULL, NULL, NULL, NULL, NULL, 'PAPUA BARAT', 'pabar', '-0.850636, 134.061052');


CREATE TABLE IF NOT EXISTS `bengkel_laphar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_bengkel` int(11) NOT NULL,
  `tgl_laporan` date NOT NULL,
  `nama` varchar(200) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `no_ac` varchar(100) NOT NULL,
  `merk_ac` varchar(100) NOT NULL,
  `pk` varchar(10) NOT NULL,
  `jenis` varchar(10) NOT NULL,
  `ampere` double DEFAULT NULL,
  `pressure` double DEFAULT NULL,
  `jenis_servis` varchar(100) NOT NULL,
  `tgl_servis_berikutnya` date DEFAULT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;