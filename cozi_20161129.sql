ALTER TABLE `bengkel_laporan` ADD `r290` INT( 11 ) NOT NULL AFTER `ac_cold` ,
ADD `r600a` INT( 11 ) NOT NULL AFTER `r290` ;

CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `bengkel_vw` AS SELECT b . * , k.id_propinsi, k.nama kotane, p.nama propinsi
FROM bengkel b
LEFT JOIN kota k ON k.id = b.id_kota
left join propinsi p on p.id = k.id_propinsi;

CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `supplier_vw` AS SELECT b . * , k.id_propinsi, k.nama kota, p.nama propinsi
FROM supplier b
LEFT JOIN kota k ON k.id = b.id_kota
left join propinsi p on p.id = k.id_propinsi;

CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `ukm_vw` AS SELECT b . * , k.id_propinsi, k.nama kota, p.nama propinsi
FROM ukm b
LEFT JOIN kota k ON k.id = b.id_kota
left join propinsi p on p.id = k.id_propinsi;

CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `besar_vw` AS SELECT b . * , k.id_propinsi, k.nama kota, p.nama propinsi
FROM besar b
LEFT JOIN kota k ON k.id = b.id_kota
left join propinsi p on p.id = k.id_propinsi;

CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `institusi_vw` AS SELECT b . * , k.id_propinsi, k.nama kota, p.nama propinsi
FROM institusi b
LEFT JOIN kota k ON k.id = b.id_kota
left join propinsi p on p.id = k.id_propinsi;

-- create pengguna_vw
CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `pengguna_vw` AS SELECT p . * , pn.id_kota, pn.id_propinsi
FROM pengguna p
LEFT JOIN pemerintah pn ON pn.id = p.id_organisasi
AND p.grup_pengguna = 'pemerintah';

-- create ukm_laporan_vw
CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `ukm_laporan_vw` AS SELECT ul . * , u.id_kota AS idkota, k.id_propinsi
FROM ukm_laporan ul
JOIN ukm u ON u.id = ul.id_ukm
JOIN kota k ON k.id = u.id_kota;

-- create besar_laporan_vw
CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `besar_laporan_vw` AS SELECT ul. * , u.id_kota AS idkota, k.id_propinsi
FROM besar_laporan ul
JOIN besar u ON u.id = ul.id_besar
JOIN kota k ON k.id = u.id_kota;

-- create bengkel_laporan_vw
CREATE OR REPLACE ALGORITHM = UNDEFINED VIEW `bengkel_laporan_vw` AS SELECT ul. * , u.id_kota AS idkota, k.id_propinsi
FROM bengkel_laporan ul
JOIN bengkel u ON u.id = ul.id_bengkel
JOIN kota k ON k.id = u.id_kota;

