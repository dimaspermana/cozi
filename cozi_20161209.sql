-- create view pengguna_instansi_vw
CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `pengguna_instansi_vw`
 AS SELECT p.*
,CASE WHEN p.grup_pengguna='pemerintah' THEN r.nama
WHEN p.grup_pengguna='bengkel' THEN b.nama
WHEN p.grup_pengguna='ukm' THEN u.nama
WHEN p.grup_pengguna='besar' THEN be.nama
WHEN p.grup_pengguna='supplier' THEN s.nama
WHEN p.grup_pengguna='institusi' THEN i.nama
WHEN p.grup_pengguna='superadmin' THEN 'SUPERADMIN'
ELSE 'PUBLIK' END instansi
FROM `pengguna` p 
LEFT JOIN pemerintah r ON r.id=p.id_organisasi AND p.grup_pengguna='pemerintah'
LEFT JOIN bengkel b ON b.id=p.id_organisasi AND p.grup_pengguna='bengkel'
LEFT JOIN ukm u ON u.id=p.id_organisasi AND p.grup_pengguna='ukm'
LEFT JOIN besar be ON be.id=p.id_organisasi AND p.grup_pengguna='besar'
LEFT JOIN supplier s ON s.id=p.id_organisasi AND p.grup_pengguna='supplier'
LEFT JOIN institusi i ON i.id=p.id_organisasi AND p.grup_pengguna='institusi'