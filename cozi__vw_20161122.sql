-- create supplier_laporan_vw
CREATE ALGORITHM = UNDEFINED VIEW `supplier_laporan_vw` AS 
SELECT ul. * , u.id_kota AS idkota, k.id_propinsi, sc.nama AS customer
FROM supplier_laporan ul
JOIN supplier u ON u.id = ul.id_supplier
JOIN supplier_customer sc ON sc.id = ul.id_customer
JOIN kota k ON k.id = u.id_kota;

-- create pengguna_vw
CREATE ALGORITHM = UNDEFINED VIEW `pengguna_vw` AS SELECT p . * , pn.id_kota, pn.id_propinsi
FROM pengguna p
LEFT JOIN pemerintah pn ON pn.id = p.id_organisasi
AND p.grup_pengguna = 'pemerintah';

-- create ukm_laporan_vw
CREATE ALGORITHM = UNDEFINED VIEW `ukm_laporan_vw` AS SELECT ul . * , u.id_kota AS idkota, k.id_propinsi
FROM ukm_laporan ul
JOIN ukm u ON u.id = ul.id_ukm
JOIN kota k ON k.id = u.id_kota;

-- create besar_laporan_vw
CREATE ALGORITHM = UNDEFINED VIEW `besar_laporan_vw` AS SELECT ul. * , u.id_kota AS idkota, k.id_propinsi
FROM besar_laporan ul
JOIN besar u ON u.id = ul.id_besar
JOIN kota k ON k.id = u.id_kota;

-- create bengkel_laporan_vw
CREATE ALGORITHM = UNDEFINED VIEW `bengkel_laporan_vw` AS SELECT ul. * , u.id_kota AS idkota, k.id_propinsi
FROM bengkel_laporan ul
JOIN bengkel u ON u.id = ul.id_bengkel
JOIN kota k ON k.id = u.id_kota;