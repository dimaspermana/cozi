<div class="panel-header">
	<h1>Pendaftaran Bengkel / Workshop</h1>
</div>
<div class="panel-body">
	<form method="post" role="form" action="#" class="form-horizontal">
		<input type="hidden" name="mawas_token" value="1137-5005-6867">
		<div class="form-group">
			<label class="control-label col-sm-4">Nama Lengkap</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nama_pengguna" placeholder="Nama Lengkap" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">No. HP</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="no_hp_pengguna" placeholder="No. HP" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Email</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="email_pengguna" placeholder="Email">
			</div>
		</div>
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-4">Nama Bengkel / Workshop</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nama_bengkel" placeholder="Nama Bengkel / Workshop" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Pemilik</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="pemilik" placeholder="Pemilik" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Jenis Usaha</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="jenis_usaha" placeholder="Jenis Usaha" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Deskripsi Usaha</label>
			<div class="col-sm-6">
				<textarea class="form-control required" name="deskripsi_usaha" placeholder="Deskripsi Usaha"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Alamat</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="alamat" placeholder="Alamat" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Kota</label>
			<div class="col-sm-6">
				<select name="id_kota" class="form-control">
					<option value="">- Pilih kota -</option>
					<?php echo modules::run('options/kota'); ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Jenis Izin Usaha</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="izin_usaha" placeholder="Jenis Izin Usaha" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">No. Registrasi Izin Usaha</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="no_registrasi" placeholder="No. Registrasi Izin Usaha" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Anggota Asosiasi dari</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="keanggotaan_asosiasi" placeholder="Anggota Asosiasi dari" required>
			</div>
		</div>
		<!--<div class="form-group">
			<label class="control-label col-sm-4">Penanggung Jawab</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="penanggung_jawab" placeholder="Penanggung Jawab" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">No. Telp. PJ</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="no_telp_pj" placeholder="No. Telp. PJ" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Email PJ</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="email_pj" placeholder="Email PJ" required>
			</div>
		</div>-->
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-4">Peralatan Service yang Dimiliki</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="peralatan" placeholder="Peralatan Service yang Dimiliki" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Jumlah Teknisi Bersetifikat BNSP</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="teknisi_bnsp" placeholder="Jumlah Teknisi Bersetifikat BNSP" required>
			</div>
		</div>
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-4 hidden-xs">&nbsp;</label>
			<div class="col-sm-6">
				<button type="submit" class="btn btn-primary">Daftar Sekarang</button>
			</div>
		</div>
	</form>
</div>