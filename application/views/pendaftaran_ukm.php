<div class="panel-header">
	<h1>Pendaftaran Foam - UKM</h1>
</div>
<div class="panel-body">
	<form method="post" role="form" action="#" class="form-horizontal">
		<input type="hidden" name="mawas_token" value="1137-5005-6867">
		<div class="form-group">
			<label class="control-label col-sm-4">Nama Lengkap</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nama_pengguna" placeholder="Nama Lengkap" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">No. HP</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="no_hp" placeholder="No. HP" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Email</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="email" placeholder="Email">
			</div>
		</div>
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-4">Nama Perusahaan / Perorangan</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nama_perusahaan" placeholder="Nama Perusahaan / Perorangan" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Alamat</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="alamat" placeholder="Alamat" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Kota</label>
			<div class="col-sm-6">
				<select name="id_kota" class="form-control required">
					<option value="">- Pilih kota -</option>
					<?php echo modules::run('options/kota'); ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">NPWP</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="npwp" placeholder="NPWP" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Kategori Produk Foam</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="kategori_produk" placeholder="Kategori Produk Foam" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Metode Pembuatan Foam</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="metode_pembuatan" placeholder="Metode Pembuatan Foam" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Supplier</label>
			<div class="col-sm-6">
				<select name="id_supplier" class="form-control required">
					<option value="">- Pilih supplier -</option>
					<?php echo modules::run('options/supplier'); ?>
				</select>
			</div>
		</div>
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-4 hidden-xs">&nbsp;</label>
			<div class="col-sm-6">
				<button type="submit" class="btn btn-primary">Daftar Sekarang</button>
			</div>
		</div>
	</form>
</div>