<div class="panel-header">
	<h1>Pendaftaran Institusi Pendidikan</h1>
</div>
<div class="panel-body">
	<form method="post" role="form" action="#" class="form-horizontal">
		<input type="hidden" name="mawas_token" value="1137-5005-6867">
		<div class="form-group">
			<label class="control-label col-sm-4">Nama Lengkap</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nama_pengguna" placeholder="Nama Lengkap" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">No. HP</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="no_hp" placeholder="No. HP" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Email</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="email" placeholder="Email">
			</div>
		</div>
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-4">Nama Institusi</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="nama_institusi" placeholder="Nama Institusi" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Profil Institusi</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="profil_institusi" placeholder="Profil Institusi" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Alat Pelatihan RAC</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="peralatan_rac" placeholder="Alat Pelatihan RAC" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Bantuan Alat yang Diterima</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="peralatan_bantuan" placeholder="Bantuan Alat yang Diterima" required>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-4">Jenis Pelatihan</label>
			<div class="col-sm-6">
				<input type="text" class="form-control" name="jenis_pelatihan" placeholder="Jenis Pelatihan" required>
			</div>
		</div>
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-4 hidden-xs">&nbsp;</label>
			<div class="col-sm-6">
				<button type="submit" class="btn btn-primary">Daftar Sekarang</button>
			</div>
		</div>
	</form>
</div>