<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
	}
	
	
	public function index()
	{
		$this->page->template('web_tpl');
		$this->page->view();
	}
	
	
	public function bengkel_num_rows($q)
	{
		$this->db->join('kota', 'bengkel.id_kota = kota.id', 'left');
		$this->db->join('propinsi', 'kota.id_propinsi = propinsi.id', 'left');
		$this->db->where("(bengkel.nama LIKE '%$q%' OR kota.nama LIKE '%$q%' OR propinsi.nama LIKE '%$q%')");
		$this->db->where("bengkel.nama != ''");
		$this->db->where('bengkel.deleted_at IS NULL');
		return $this->db->count_all_results('bengkel');
	}
	
	
	public function workshop($q = 'x', $page = '1')
	{
		$q = base64_decode($q);
		$q = (mb_detect_encoding($q) == 'ASCII') ? $q : '';
		
		$offset = ($page - 1) * 20;
		
		$this->db->select('bengkel.*, kota.nama AS kota');
		$this->db->join('kota', 'bengkel.id_kota = kota.id', 'left');
		$this->db->join('propinsi', 'kota.id_propinsi = propinsi.id', 'left');
		$this->db->where("bengkel.nama != ''");
		$this->db->where('bengkel.deleted_at IS NULL');
		$this->db->where("(bengkel.nama LIKE '%$q%' OR kota.nama LIKE '%$q%' OR propinsi.nama LIKE '%$q%')");
		$this->db->order_by('bengkel.nama, kota');
		$this->db->limit(20, $offset);
		$bengkel = $this->db->get('bengkel');
		
		$this->page->template('page_tpl');
		$this->page->view(NULL, array (
			'pages' => ceil($this->bengkel_num_rows($q) / 20),
			'page' => $page,
			'search' => $q,
			'bengkel' => $bengkel,
		));
	}
	
	
	public function search_workshop()
	{
		
		$q = base64_encode($this->input->post('search'));
		redirect(site_url("/site/workshop/$q"));
	}
	
	
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */