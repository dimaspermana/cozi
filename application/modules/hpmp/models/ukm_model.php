<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ukm_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'ukm_laporan_vw';
		
		$this->like = array();
		$this->filter = array (
			'id_ukm' => (user_session('grup_pengguna') == 'ukm') ? user_session('id_organisasi') : '',
			'idkota' => (user_session('tingkatan') == '3') ? user_session('id_kota') : '',
			'id_propinsi' => (user_session('tingkatan') == '2') ? user_session('id_propinsi') : '',
		);
		
		$this->fields = (object) array (
			'id_ukm' => '',
			'tahun' => date('Y'),
			'bulan' => str_pad(date('m'), 2, '0', STR_PAD_LEFT),
			'pembelian_preblended' => ''
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, CONCAT(tahun, '-', bulan) AS tahun_bulan, b.nama AS ukm, c.nama AS pengguna", FALSE);
		$this->db->join("ukm AS b", "$main_table.id_ukm = b.id", 'left');
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
}
/* End of file ukm_model.php */
/* Location: ./application/modules/hpmp/models/ukm_model.php */