<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('supplier_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->supplier_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'tahun_bulan' => array('text' => 'Periode Lap.'),
				'created_at' => array('text' => 'Waktu Input'),
				'supplier' => array('text' => 'Nama Supplier'),
				'pengguna' => array('text' => 'Pengguna Input'),
				'customer' => array('text' => 'Customer'),
				'produk' => array('text' => 'Produk', 'func' => 'jenis_produk'),
				'penjualan' => array('text' => 'Penjualan'),
			),
			'num_rows' => $this->supplier_model->num_rows(),
			'item' => 'tahun_bulan',
			'order' => 'desc',
			'warning' => 'tahun_bulan',
			'checkbox' => FALSE,
		));
		
		$this->supplier_model->set_grid_params($this->grid->params());
		$this->grid->source($this->supplier_model->get());
		
		$this->page->view('supplier_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->supplier_model->filter,
			'keyword' => $this->supplier_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('supplier_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->supplier_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('id_supplier','id_customer','tahun', 'bulan', 'produk', 'penjualan');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$data = $this->form_data();
		db_insert('supplier_laporan', $data);
		redirect($this->input->post('redirect'));
	}
	
	
	public function update($id)
	{
		$data = $this->form_data();
		db_update('supplier_laporan', $data, array('id' => $id));
		redirect($this->input->post('redirect'));
	}
	
	
	public function profil($id)
	{
		$this->page->view('supplier_profil', array (
			'data' => $this->supplier_model->by_id($id),
		));
	}

	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('supplier_laporan', array('id' => $id));
		redirect($this->agent->referrer());
	}

	function get_customer()
	{
		$id_supplier = $this->input->get('id_supplier');

		$this->db->select("*", FALSE);
		$this->db->where("deleted_at IS NULL AND id_supplier='$id_supplier'");
		$src = $this->db->order_by('nama')->get('supplier_customer')->result();
		
		echo json_encode($src);
	}

}

/* End of file supplier.php */
/* Location: ./application/modules/hpmp/controllers/supplier.php */