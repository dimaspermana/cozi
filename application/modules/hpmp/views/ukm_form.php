<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Bulanan Foam - UKM</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-4"><span class="red">*</span> UKM </label>
				<div class="col-sm-6">
					<select class="form-control required" name="id_ukm">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih UKM -</option>
						<?php endif; ?>
						<?php echo modules::run('options/ukm', $data->id_ukm); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Tahun Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="tahun">
						<?php echo modules::run('options/tahun', $data->tahun); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Bulan Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="bulan">
						<?php echo modules::run('options/bulan', $data->bulan); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<!--<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 1/2 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_05_pk" value="<?php echo $data->ac_05_pk; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 3/4 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_075_pk" value="<?php echo $data->ac_075_pk; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 1 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_1_pk" value="<?php echo $data->ac_1_pk; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 2 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_2_pk" value="<?php echo $data->ac_2_pk; ?>">
				</div>
			</div>-->
			<!-- <div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 1/2 - 2PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_05_2" value="<?php echo $data->ac_05_2; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 3 - 10 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_3_10" value="<?php echo $data->ac_3_10; ?>">
				</div>
			</div> -->
			<div class="form-group">
				<label class="control-label col-sm-5">Pembelian Preblended</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="pembelian_preblended" value="<?php echo $data->pembelian_preblended; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>