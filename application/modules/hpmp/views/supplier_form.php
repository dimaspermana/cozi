<script type="text/javascript">
	function get_customer()
	{
		var supplier = $("#id_supplier").val();

		$.ajax({
			data: {'id_supplier':supplier},
			dataType : 'json',
			type : 'get',
			url : "<?=$this->page->base_url('get_customer')?>",
			success : function(hasil)
			{
				console.log(hasil);
				$opt = '<option value="">- Pilih Customer -</option>';
				$.each(hasil, function(index, data){
					$opt += '<option value="'+data['id']+'">'+data['nama']+'</option>';
				});

				$("#id_customer").html($opt);
			}
		});
	}
</script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Bulanan Foam - System House</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-4"><span class="red">*</span> Foam - System House</label>
				<div class="col-sm-6">
					<select class="form-control required" name="id_supplier" id="id_supplier" onchange="get_customer();">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih System House -</option>
						<?php endif; ?>
						<?php echo modules::run('options/supplier_', $data->id_supplier); ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4"><span class="red">*</span> Customer</label>
				<div class="col-sm-6">
					<select class="form-control required" name="id_customer" id="id_customer">
						<option value="">- Pilih Customer -</option>
						<?php echo modules::run('options/customer', $data->id_customer, $data->id_supplier); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Tahun Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="tahun">
						<?php echo modules::run('options/tahun', $data->tahun); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Bulan Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="bulan">
						<?php echo modules::run('options/bulan', $data->bulan); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			
			<div class="form-group">
				<label class="control-label col-sm-5">Produk</label>
				<div class="col-sm-7">
					<select class="form-control required" name="produk">
					<?php 
						$hcfc='';
						$preblended='';
						if($data->produk=='hcfc'){
							$hcfc='selected';
						}elseif ($data->produk=='preblended') {
							$preblended='selected';
						}
					?>
					<option value="hcfc" <?php echo $hcfc;?> >HCFC</option>
					<option value="preblended" <?php echo $preblended;?>>Preblended</option>
					</select>
				</div>
			</div>
			
		</div>
		<div class="col-md-6">
			
			<div class="form-group">
				<label class="control-label col-sm-5">Penjualan</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="penjualan" value="<?php echo $data->penjualan; ?>">
				</div>
			</div>
			
		</div>
		
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>