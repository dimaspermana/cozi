<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Bulanan Bengkel / Workshop</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-4"><span class="red">*</span> Bengkel / Workshop</label>
				<div class="col-sm-6">
					<select class="form-control required" name="id_bengkel">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih Bengkel / Workshop -</option>
						<?php endif; ?>
						<?php echo modules::run('options/bengkel', $data->id_bengkel); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Tahun Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="tahun">
						<?php echo modules::run('options/tahun', $data->tahun); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Bulan Laporan</label>
				<div class="col-sm-7">
					<select class="form-control required" name="bulan">
						<?php echo modules::run('options/bulan', $data->bulan); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<!--<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 1/2 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_05_pk" value="<?php echo $data->ac_05_pk; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 3/4 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_075_pk" value="<?php echo $data->ac_075_pk; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 1 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_1_pk" value="<?php echo $data->ac_1_pk; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 2 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_2_pk" value="<?php echo $data->ac_2_pk; ?>">
				</div>
			</div>-->
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 1/2 - 2PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_05_2" value="<?php echo $data->ac_05_2; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC 3 - 10 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_3_10" value="<?php echo $data->ac_3_10; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC > 10 PK (unit)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_10" value="<?php echo $data->ac_10; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<!--<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service AC Mobil</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_mobil" value="<?php echo $data->ac_mobil; ?>">
				</div>
			</div>-->
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service Kulkas</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_kulkas" value="<?php echo $data->ac_kulkas; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service Cold Storage</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_cold" value="<?php echo $data->ac_cold; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jumlah Service Lainnya</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ac_lainnya" value="<?php echo $data->ac_lainnya; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-22 (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r22" value="<?php echo $data->r22; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-123 (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r123" value="<?php echo $data->r123; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-134A (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r134a" value="<?php echo $data->r134a; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-32 (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r32" value="<?php echo $data->r32; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-410A (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r410a" value="<?php echo $data->r410a; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-404A (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r404a" value="<?php echo $data->r404a; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-407A (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r407a" value="<?php echo $data->r407a; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-290(kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r290" value="<?php echo $data->r290; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan R-600A(kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="r600a" value="<?php echo $data->r600a; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan (R-717) (ton)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="amonia" value="<?php echo $data->amonia; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Penggunaan (R-744) (kg)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="co2" value="<?php echo $data->co2; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>