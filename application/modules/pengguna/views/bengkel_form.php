<script type="text/javascript">
$().ready(function() {
	get_pengguna();
});
	function get_pengguna()
	{
		var pengguna = $("#grup_pengguna").val();
		var id_organisasi = $("#idorg").val();;
		if(pengguna!='superadmin' && pengguna!='publik' && pengguna!=''){
		$.ajax({
			data: {'pengguna':pengguna},
			dataType : 'json',
			type : 'get',
			url : "<?=$this->page->base_url('get_pengguna')?>",
			success : function(hasil)
			{
				// console.log(hasil);
				$opt = '<option value="">- Pilih '+pengguna+' -</option>';
				$.each(hasil, function(index, data){
					if(data['id']==id_organisasi){
					$opt += '<option value="'+data['id']+'" selected>'+data['nama']+'</option>';
					}else{						
					$opt += '<option value="'+data['id']+'">'+data['nama']+'</option>';
					}

				});


				$("#id_organisasi").html($opt);
				// $("#level").html(pengguna);
				$("#organisasi").show();

			}
		});
		}else{
			$("#id_organisasi").html("");
			$("#organisasi").hide();
		}
	}
</script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Manajemen Pengguna</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<input type="hidden" id="idorg" value="<?php echo $data->id_organisasi; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Nama Pengguna</label>
				<div class="col-sm-6">
					<input type="text" class="form-control required" name="nama" placeholder="Nama Pengguna" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>No HP</label>
				<div class="col-sm-6">
					<input type="text" class="form-control required" name="no_hp" placeholder="No. HP" value="<?php echo $data->no_hp; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-12">
		<div class="form-group">
				<label class="control-label col-sm-2"></label>
		<div class="form-submit col-md-8" style="text-align: left !important;" >
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
		</div>
		</div>
	</form>
</div>