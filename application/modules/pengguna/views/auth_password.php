<script src="<?php echo base_url('/evt/password.form.js'); ?>"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Ganti Password</h1>
</div>
<div id="main-container" class="container-fluid">
	<?php if ($this->session->flashdata('update_status') == 'success'): ?>
	<div class="alert alert-success">Password berhasil diganti.</div>
	<?php endif; ?>
	
	<?php if ($this->session->flashdata('update_status') == 'failed'): ?>
	<div class="alert alert-danger">Password lama salah.</div>
	<?php endif; ?>
	
	<form class="form-horizontal validate-form" method="post" action="<?php echo site_url('/pengguna/auth/update_pwd'); ?>">
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-4">Password Lama</label>
				<div class="col-sm-7">
					<input type="password" class="form-control required" placeholder="Password Lama" name="old_password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Password Baru</label>
				<div class="col-sm-7">
					<input type="password" class="form-control required" placeholder="Password Baru" name="new_password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Ulangi Password Baru</label>
				<div class="col-sm-7">
					<input type="password" class="form-control required" placeholder="Ulangi Password Baru" name="retype_password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4 hidden-xs"></label>
				<div class="col-sm-7">
					<button type="submit" class="btn btn-success">Simpan Password</button>
				</div>
			</div>
		</div>
	</form>
</div>