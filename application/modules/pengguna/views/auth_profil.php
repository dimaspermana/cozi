<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Profil Saya</h1>
</div>
<div id="main-container" class="container-fluid">
	<?php if ($this->session->flashdata('update_status') == 'success'): ?>
	<div class="alert alert-success">Data berhasil disimpan.</div>
	<?php endif; ?>
	
	<form class="form-horizontal validate-form" method="post" action="<?php echo site_url('/pengguna/auth/update_profile'); ?>">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-4">No. Anggota</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" placeholder="No. Anggota" name="no_anggota" value="<?php echo user_session('no_anggota'); ?>" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Nama Lengkap</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" placeholder="Nama Lengkap" name="nama" value="<?php echo user_session('nama'); ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">No. Identitas (KTP)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" placeholder="No. Identitas (KTP)" name="no_identitas" value="<?php echo user_session('no_identitas'); ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Jenis Kelamin</label>
				<div class="col-sm-7">
					<label class="radio-inline"><input type="radio" name="jenis_kelamin" value="L" <?php if (user_session('jenis_kelamin') == 'L') echo 'checked'; ?> >Laki-laki</label>
					<label class="radio-inline"><input type="radio" name="jenis_kelamin" value="P" <?php if (user_session('jenis_kelamin') == 'P') echo 'checked'; ?> >Perempuan</label>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Tempat Lahir</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" value="<?php echo user_session('tempat_lahir'); ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Tgl. Lahir</label>
				<div class="col-sm-7">
					<input type="text" class="form-control datepicker" placeholder="Tgl. Lahir" name="tgl_lahir" value="<?php echo user_session('tgl_lahir'); ?>" readonly>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-4">Alamat</label>
				<div class="col-sm-7">
					<textarea class="form-control" placeholder="Alamat" name="alamat"><?php echo user_session('alamat'); ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Kota</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" placeholder="Kota" name="kota" value="<?php echo user_session('kota'); ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">No. HP</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" placeholder="No. HP" name="no_hp" value="<?php echo user_session('no_hp'); ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Email</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo user_session('email'); ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Pekerjaan</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" placeholder="Pekerjaan" name="pekerjaan" value="<?php echo user_session('pekerjaan'); ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Penghasilan per Bulan</label>
				<div class="col-sm-7">
					<input type="text" class="form-control number" placeholder="Penghasilan per Bulan" name="penghasilan" value="<?php echo user_session('penghasilan'); ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-default">Simpan Profil</button>
		</div>
	</form>
</div>