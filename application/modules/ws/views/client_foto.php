<!DOCTYPE HTML>
<html>
<head>
	<script src="<?php echo base_url(); ?>js/jquery-1.12.4.min.js"></script>
	<script>
	$().ready(function() {
		
		$('form').submit(function(e) {
			e.preventDefault();
			
			var action = $('form').attr('action');
			var data = new FormData($('form')[0]);
			
			$.ajax ({
				url: action,
				type: 'POST',
				data: data,
				async: false,
				cache: false,
				contentType: false,
				processData: false
			}).done(function() {
				console.log('Berhasil dikirim');
			}).fail(function() {
				console.log('Gagal dikirim');
			});
		});
		
	});
	</script>
</head>
<body>
	<form method="post" action="<?php echo site_url('/ws/bengkel/uploadImage'); ?>" enctype="multipart/form-data">
		<input type="hidden" name="mawas_token" value="1137-5005-6867">
		<input type="hidden" name="id_pengguna" value="1">
		<input type="hidden" name="id_organisasi" value="123">
		<input type="hidden" name="keterangan" value="Ujicoba Upload Foto">
		<input type="hidden" name="latitude" value="0.123">
		<input type="hidden" name="longitude" value="-2.345">
		<input type="file" name="foto">
		<input type="submit" value="Kirim">
	</form>
</body>
</html>