<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Besar_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'besar_laporan_vw';
		
		$this->like = array();
		
		if (user_session('grup_pengguna') == 'besar'){
		$this->filter = array (
			'id_besar' => (user_session('grup_pengguna') == 'besar') ? user_session('id_organisasi') : '',
		);
		}
		else if((user_session('grup_pengguna') == 'pemerintah') && ((user_session('tingkatan') == '3'))){
			$this->filter = array (
			'idkota' => user_session('id_kota'),
		);
		}
		else if((user_session('grup_pengguna') == 'pemerintah') && ((user_session('tingkatan') == '2'))){
			$this->filter = array (
			'id_propinsi' => user_session('id_propinsi'),
		);
		}
		else {
			
		}
		
		$this->fields = (object) array (
			'id_besar' => '',
			'tahun' => date('Y'),
			'bulan' => str_pad(date('m'), 2, '0', STR_PAD_LEFT),
			'pembelian_hcfc' => '',
			'pembelian_preblended' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, CONCAT(tahun, '-', bulan) AS tahun_bulan, b.nama AS besar, c.nama AS pengguna", FALSE);
		$this->db->join("besar AS b", "$main_table.id_besar = b.id", 'left');
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->order_by($this->order.', created_at DESC, besar');
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */