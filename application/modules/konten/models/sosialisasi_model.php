<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sosialisasi_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'sosialisasi';
		
		$this->like = array();
		$this->filter = array ();
		
		$this->fields = (object) array (
			'id_institusi' => '',
			'kegiatan' => '',
			'tgl_sosialisasi' => '',
			'tempat' => '',
			'artikel' => '',
			'jenis_media' => '',
			'nama_file' => '',
			'id_kota' => '',
			'kota' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*,k.id id_kota,k.nama kota ,c.nama AS pengguna",FALSE);
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->join("kota AS k", "$main_table.id_kota = k.id", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}

	public function get_profil($id){

		$main_table = $this->table;

		$this->db->select("$main_table.*,k.nama kota,m.jenis_media,m.nama_file,m.url",FALSE);
		$this->db->join("sosialisasi_foto AS m","$main_table.id = m.id_sosialisasi","left");
		$this->db->join("kota AS k","$main_table.id_kota = k.id","left");
		$this->db->where("$main_table.id", $id);
		$this->db->where("m.deleted_at IS NULL");
		$src = $this->db->get($main_table);
		return $src->num_rows() > 0 ? $src->row() : $this->fields;

	}

	public function get_picture($id){
		$query = "SELECT * FROM sosialisasi_foto WHERE deleted_at IS NULL AND id_sosialisasi = $id ORDER BY id DESC";

    	$src = $this->db->query($query);
    	return $src;
	}
}