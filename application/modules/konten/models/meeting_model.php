<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meeting_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'meeting';
		
		$this->like = array();
		$this->filter = array ();
		
		$this->fields = (object) array (
			'id_institusi' => '',
			'kegiatan' => '',
			'tgl_meeting' => '',
			'tempat' => '',
			'artikel' => '',
			'jenis_media' => '',
			'nama_file' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, c.nama AS pengguna",FALSE);
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}

	public function get_profil($id){

		$main_table = $this->table;

		$this->db->select("$main_table.*, m.jenis_media,m.nama_file,m.url",FALSE);
		$this->db->join("meeting_foto AS m","$main_table.id = m.id_meeting","left");
		$this->db->where("$main_table.id", $id);
		$this->db->where("m.deleted_at IS NULL");
		$src = $this->db->get($main_table);
		return $src->num_rows() > 0 ? $src->row() : $this->fields;

	}

	public function get_picture($id){
		$query = "SELECT * FROM meeting_foto WHERE deleted_at IS NULL AND id_meeting = $id ORDER BY id DESC";

    	$src = $this->db->query($query);
    	return $src;
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */