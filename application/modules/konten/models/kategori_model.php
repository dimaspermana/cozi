<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'pengaduan_kategori';
		
		$this->like = array();
		$this->filter = array (
			'nama' => (user_session('grup_pengguna') == 'institusi') ? user_session('nama') : '',
		);
		
		$this->fields = (object) array (
			'id' => '',
			'tgl_posting' => '',
			'nama' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, nama AS pengguna",FALSE);
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
}
/* End of file kategori_model.php */
/* Location: ./application/modules/hpmp/models/kategori_model.php */