<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tips_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'tips';
		
		$this->like = array();
		$this->filter = array (
			'id_institusi' => (user_session('grup_pengguna') == 'institusi') ? user_session('id_organisasi') : '',
		);
		
		$this->fields = (object) array (
			'id_institusi' => '',
			'judul' => '',
			'tgl_publish' => '',
			'artikel' => '',
			'status' => '',
			'nama_file' => '',
			'url' => '',
			'pengguna' => '',
			'jenis_media' => '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, c.nama AS pengguna",FALSE);
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */