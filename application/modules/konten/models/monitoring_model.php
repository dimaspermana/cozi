<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monitoring_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'monitoring';
		
		$this->like = array();
		$this->filter = array (
			$this->table.'.id_organisasi' => (user_session('tingkatan') == '4') ? user_session('id_organisasi') : '',
		);
		
		$this->fields = (object) array (
			'id_organisasi' 	=> '',
			'tgl_posting' 		=> '',
			'tgl_monitoring' 	=> '',
			'perusahaan' 		=> '',
			'keterangan' 		=> '',
		);
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*, c.nama AS pengguna, p.nama AS nama_perusahaan",FALSE);
		$this->db->join("pengguna AS c", "$main_table.created_by = c.id", 'left');
		$this->db->join("perusahaan_vw AS p", "$main_table.id_organisasi = p.id AND $main_table.perusahaan=p.perusahaan", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}

	public function get_profil($id){

		$main_table = $this->table;

		$this->db->select("$main_table.*, m.nama_file, p.nama AS nama_perusahaan",FALSE);
		$this->db->join("monitoring_foto AS m","$main_table.id = m.id_monitoring","left");
		$this->db->join("perusahaan_vw AS p","$main_table.id_organisasi = p.id","left");
		$this->db->where("$main_table.id", $id);
		$this->db->where("m.deleted_at IS NULL");
		$src = $this->db->get($main_table);
		return $src->num_rows() > 0 ? $src->row() : $this->fields;

	}

	public function get_picture($id){
		$query = "SELECT * FROM monitoring_foto WHERE deleted_at IS NULL AND id_monitoring = $id ORDER BY id DESC";

    	$src = $this->db->query($query);
    	return $src;
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/hpmp/models/bengkel_model.php */