<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monitoring extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('monitoring_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->monitoring_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at' 		=> array('text' => 'Waktu Input'),
				'nama_perusahaan' 	=> array('text' => 'Nama', 'link' => $this->page->base_url('/profil')),
				'perusahaan' 		=> array('text' => 'Perusahaan', 'func' => 'to_upper'),
				'tgl_monitoring' 	=> array('text' => 'Tanggal Monitoring'),
				'pengguna' 	=> array('text' => 'Pengguna Input'),
			),
			'num_rows' => $this->monitoring_model->num_rows(),
			'item' => 'created_at',
			'order' => 'desc',
			'warning' => 'created_at',
			'checkbox' => FALSE,
		));
		
		$this->monitoring_model->set_grid_params($this->grid->params());
		$this->grid->source($this->monitoring_model->get());
		
		if(user_session('tingkatan')!=1 && user_session('tingkatan')!=2 && user_session('tingkatan')!=3){	
		$this->grid->add_actions(array (
			'hapus' =>  array('show' => FALSE),
			'ubah' =>  array('show' => FALSE),
		));
		}


		$this->page->view('monitoring_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->monitoring_model->filter,
			'keyword' => $this->monitoring_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('monitoring_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->monitoring_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('id_organisasi', 'tgl_monitoring', 'keterangan');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$data = $this->form_data();
		$words = explode('_', $this->input->post('id_organisasi'));
		$data['id_organisasi'] 	= $words[0];
		$data['perusahaan'] 	= $words[1];

	    db_insert('monitoring', $data);
		$id_adu = $this->db->insert_id();


		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){

		} else if(isset($_FILES['filename'])){

			$this->upload($id_adu);
		}	

		redirect($this->input->post('redirect'));
	}
	
	function upload($id){

		$upload_path = './img/monitoring/'.$id;

		if ( ! file_exists($upload_path)) mkdir($upload_path, 0777); 

        if(!empty($_FILES['filename']['name'])){

			$filesCount = count($_FILES['filename']['name']);
            for($i = 0; $i < $filesCount; $i++){

				$_FILES['userFile']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['filename']['size'][$i];

                $config['upload_path'] = $upload_path;
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['encrypt_name'] = TRUE;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){

					$fileData = $this->upload->data();

					$data_foto = array('id_monitoring'=> $id, 'nama_file' => $fileData['file_name']);

                    db_insert("monitoring_foto",$data_foto);
                }
            }
            
        }		

	}

	public function update($id)
	{
		$data = $this->form_data();
		$words = explode('_', $this->input->post('id_organisasi'));
		$data['id_organisasi'] 	= $words[0];
		$data['perusahaan'] 	= $words[1];

		db_update('monitoring', $data, array('id' => $id));

		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){
		    
		} else if(isset($_FILES['filename'])){
			db_delete('monitoring_foto',array("id_monitoring" => $id));
			$this->upload($id);
		}
		// }

		redirect($this->input->post('redirect'));
	}
	
	
	public function profil($id)
	{
		$this->page->view('monitoring_profil', array (
			// 'data' => $this->monitoring_model->by_id($id),
			'data' => $this->monitoring_model->get_profil($id),
			'pict' => $this->monitoring_model->get_picture($id),
		));
	}

	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('monitoring', array('id' => $id));
		db_delete('monitoring_foto', array('id_monitoring' => $id));
		redirect($this->agent->referrer());
	}
	
}

/* End of file pengaduan.php */
/* Location: ./application/modules/meeting/controllers/pengaduan.php */