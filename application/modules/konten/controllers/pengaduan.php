<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengaduan extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('pengaduan_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->pengaduan_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at' => array('text' => 'Waktu Input'),
				'judul' => array('text' => 'Judul', 'link' => $this->page->base_url('/profil')),
				'kategori' => array('text' => 'Kategori'),
				'status' => array('text' => 'Status', 'func' => 'status_pengaduan', 'link' => $this->page->base_url('/status')),
				'lokasi' => array('text' => 'Lokasi', 'align' => 'center', 'func' => 'cek_lokasi'),
				'pengguna' => array('text' => 'Pengguna Input'),
			),
			'num_rows' => $this->pengaduan_model->num_rows(),
			'item' => 'created_at',
			'order' => 'desc',
			'warning' => 'created_by',
			'checkbox' => FALSE,
		));

		if(user_session('grup_pengguna')!='superadmin'){
			$this->grid->init(array(
			 'user_id' => user_session('id'),
			));
		}
		
		// $this->grid->add_actions (array (
		// 	'status' => array('show' => TRUE, 'title' => 'STATUS', 'icon' => 'ok-circle'),
		// ));
		
		/*$this->grid->init(array(
			 'user_id' => user_session('id'),
			));*/

		$this->pengaduan_model->set_grid_params($this->grid->params());
		$this->grid->source($this->pengaduan_model->get());
		

		$this->page->view('pengaduan_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->pengaduan_model->filter,
			'keyword' => $this->pengaduan_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	public function status($id)
	{
		if ($this->agent->referrer() == '') show_404();
		$cek = $this->db->get_where('pengaduan',  array('id' =>$id))->row()->status;
		$value_status = '1';
		if ($cek == '1')
			{
				{
					$value_status = '0';
				}
				
			}
			
		db_update('pengaduan', array('status' => $value_status), array('id' => $id));
		redirect($this->agent->referrer());
	}

	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('pengaduan_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->pengaduan_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('upper_judul', 'id_kategori', 'lokasi', 'keterangan');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$data = $this->form_data();

	    db_insert('pengaduan', $data);
		$id_adu = $this->db->insert_id();


		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){
		    
		} else if(isset($_FILES['filename'])){ 

			$this->upload($id_adu);
		}	

		redirect($this->input->post('redirect'));
	}
	
	function upload($id){

		$upload_path = './img/pengaduan/'.$id;

		if ( ! file_exists($upload_path)) mkdir($upload_path, 0777); 
		
		$data = array();
        if(!empty($_FILES['filename']['name'])){
            $filesCount = count($_FILES['filename']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['filename']['size'][$i];

                $config['upload_path'] = $upload_path;
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['encrypt_name'] = TRUE;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                  
                  	$data_foto = array('id_pengaduan'=> $id, 'nama_file' => $fileData['file_name']);

                    db_insert("pengaduan_foto",$data_foto);
                }
            }
            
        }		

	}

	public function update($id)
	{

		$data = $this->form_data();
		db_update('pengaduan', $data, array('id' => $id));

		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){
		    
		} else if(isset($_FILES['filename'])){ 
			db_delete('pengaduan_foto',array("id_pengaduan" => $id));
			$this->upload($id);
		}
		// }

		redirect($this->input->post('redirect'));
	}
	
	
	public function profil($id)
	{
		$this->page->view('pengaduan_profil', array (
			// 'data' => $this->pengaduan_model->by_id($id),
			'data' => $this->pengaduan_model->get_profil($id),
			'pict' => $this->pengaduan_model->get_picture($id),
		));
	}

	public function hapus($id)
	{

		if ($this->agent->referrer() == '') show_404();
		db_delete('pengaduan', array('id' => $id));
		db_delete('pengaduan_foto', array('id_pengaduan' => $id));
		redirect($this->agent->referrer());
	}
	
}

/* End of file pengaduan.php */
/* Location: ./application/modules/meeting/controllers/pengaduan.php */