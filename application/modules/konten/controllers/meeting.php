<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meeting extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('meeting_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->meeting_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at' => array('text' => 'Waktu Input'),
				'kegiatan' => array('text' => 'Kegiatan', 'link' => $this->page->base_url('/profil')),
				'tgl_meeting' => array('text' => 'Tanggal Meeting'),
				'tempat' => array('text' => 'Tempat'),
				'pengguna' => array('text' => 'Pengguna Input'),
			),
			'num_rows' => $this->meeting_model->num_rows(),
			'item' => 'created_at',
			'order' => 'desc',
			'warning' => 'created_by',
			'checkbox' => FALSE,
		));

		if(user_session('grup_pengguna')!='superadmin'){
			$this->grid->init(array(
			 'user_id' => user_session('id'),
			));
		}
		
		$this->meeting_model->set_grid_params($this->grid->params());
		$this->grid->source($this->meeting_model->get());
		
		if(user_session('tingkatan')!=1 && user_session('tingkatan')!=2 && user_session('tingkatan')!=3){	
		$this->grid->disable_all_acts();
		}


		$this->page->view('meeting_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->meeting_model->filter,
			'keyword' => $this->meeting_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('meeting_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->meeting_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('upper_kegiatan', 'tgl_meeting','upper_tempat','artikel');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$filename = $this->input->post("filename");
		$data = $this->form_data();
		$video = $this->input->post('videosrc');
		$media = $this->input->post('media');
		// print_r($data);
		// $data['tgl_pelatihan'] = $tgl;


	    db_insert('meeting', $data);
		$id_meet = $this->db->insert_id();


		if(! empty($video)){
			foreach ($video as $key => $value) {
				db_insert('meeting_foto',array("id_meeting" => $id_meet, "url"=> $value, "jenis_media"=> '1') );
			}
		}
		

		// // if($media == 0)
		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){
		    
		} else if(isset($_FILES['filename'])){ 

			$this->upload($id_meet);
		}	

		redirect($this->input->post('redirect'));
	}
	
	function upload($id){

		$upload_path = './img/meeting/'.$id;

		if ( ! file_exists($upload_path)) mkdir($upload_path, 0777); 
		
		$data = array();
        if(!empty($_FILES['filename']['name'])){
            $filesCount = count($_FILES['filename']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['filename']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['filename']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['filename']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['filename']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['filename']['size'][$i];

                $uploadPath = 'uploads/files/';
                $config['upload_path'] = $upload_path;
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['encrypt_name'] = TRUE;
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                  
                  	$data_foto = array('id_meeting'=> $id, 'nama_file' => $fileData['file_name'],"jenis_media"=> '0');

                    db_insert("meeting_foto",$data_foto);
                }
            }
            
        }		

	}

	public function update($id)
	{
		// phpinfo(38);die();
		$video = $this->input->post('videosrc');
		$media = $this->input->post('media');

		$data = $this->form_data();
		db_update('meeting', $data, array('id' => $id));
		
		if(! empty($video)){
			db_delete('meeting_foto',array("id_meeting" => $id, "jenis_media"=> '1'));
			// print_r($video);
			// die();
			foreach ($video as $key => $value) {
				
				db_insert('meeting_foto',array("id_meeting" => $id, "url"=> $value, "jenis_media"=> '1') );
			}
		}
		
		// if($media == 0){

		if(isset($_FILES['filename']) && count($_FILES['filename']['error']) == 1 && $_FILES['filename']['error'][0] > 0){
		    
		} else if(isset($_FILES['filename'])){ 
			db_delete('meeting_foto',array("id_meeting" => $id,"jenis_media"=> '0'));
			$this->upload($id);
		}
		// }

		redirect($this->input->post('redirect'));
	}
	
	
	public function profil($id)
	{
		$this->page->view('meeting_profil', array (
			// 'data' => $this->meeting_model->by_id($id),
			'data' => $this->meeting_model->get_profil($id),
			'pict' => $this->meeting_model->get_picture($id), 
		));
	}

	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('meeting', array('id' => $id));
		db_delete('meeting_foto', array('id_meeting' => $id));
		redirect($this->agent->referrer());
	}
	
}

/* End of file meeting.php */
/* Location: ./application/modules/meeting/controllers/meeting.php */