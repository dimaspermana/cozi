<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('kategori_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->kategori_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at' => array('text' => 'Waktu Input'),
				'nama' => array('text' => 'Kategori'),
			),
			'num_rows' => $this->kategori_model->num_rows(),
			'item' => 'nama',
			'order' => 'desc',
			'warning' => 'nama',
			'checkbox' => FALSE,
		));

		if(user_session('grup_pengguna')!='superadmin'){
			$this->grid->init(array(
			 'user_id' => user_session('id'),
			));
		}
		
		$this->kategori_model->set_grid_params($this->grid->params());
		$this->grid->source($this->kategori_model->get());
		

		$this->page->view('kategori_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->kategori_model->filter,
			'keyword' => $this->kategori_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('kategori_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->kategori_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('upper_nama');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$data = $this->form_data();

	    db_insert('pengaduan_kategori', $data);
		$id_adu = $this->db->insert_id();

		redirect($this->input->post('redirect'));
	}

	public function update($id)
	{
		$data = $this->form_data();
		db_update('pengaduan_kategori', $data, array('id' => $id));

		redirect($this->input->post('redirect'));
	}	
	
	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('pengaduan_kategori', array('id' => $id));
		redirect($this->agent->referrer());
	}
	
}

/* End of file pengaduan_kategori.php */
/* Location: ./application/modules/meeting/controllers/pengaduan_kategori.php */