
	<script type="text/javascript" src="<?php echo base_url()?>fancyBox/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>fancyBox/source/jquery.fancybox.css" media="screen" />

	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();		
		});
	</script>
	<style type="text/css" media="screen">
        .foto{
            width: 100%;
            height: 200px;
        }
    </style>
	
	<div id="header" class="container-fluid">
	<h1 class="col-md-6"><img src="<?php echo base_url(); ?>img/meet.png">&nbsp; <?php echo $data->kegiatan; ?></h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-2">
		<dl class="profile">
			<dt>Tanggal Publish</dt>
			<dd><?php echo $data->created_at; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Tanggal Meeting</dt>
			<dd><?php echo $data->tgl_meeting; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Tempat</dt>
			<dd><?php echo $data->tempat; ?></dd>
		</dl>
	<div class="separator2"></div>
	</div>
	<div class="col-sm-9">
		<div class="row">
			<?php if ($pict->num_rows() == 0): ?>
				<div class="alert alert-warning">Belum ada foto.</div>
			<?php else: ?>
			<?php
          		foreach ($pict->result() as $res) {
        	?>
				<div class="col-md-3">
					<?php if($res->jenis_media==0): ?>
					<a class="fancybox fancybox.image" data-fancybox-group="data" href="<?php echo base_url("/img/meeting/{$res->id_meeting}/{$res->nama_file}"); ?>">
						<img src="<?php echo base_url("/img/meeting/{$res->id_meeting}/{$res->nama_file}"); ?>" class="img-responsive foto">
					</a>
					<?php else: ?>
					<a class="fancybox fancybox.iframe" data-fancybox-group="data" href="<?php echo $res->url;?>" >
						<iframe src="<?php echo $res->url;?>" class="img-responsive" style="margin: 0 20px;"></iframe>
					</a>
					<?php endif; ?>
				</div>
        	<?php
	          }
	        ?>
	        <?php endif; ?>
		</div>
		<div class="clearfix"></div>
		<div class="separator2"></div>
		<div class="col-sm-12">
		<?php echo str_replace("\n","<br>",$data->artikel); ?>
		</div>
	</div>
</div>