<link rel="stylesheet" href="<?php echo base_url()?>js/zebra_datepicker/public/css/default.css" type="text/css">  
<link rel="stylesheet" href="<?php echo base_url()?>css/magic-check.css" type="text/css">  

<script type="text/javascript" src="<?php echo base_url()?>js/jquery.form.min.js"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Kategori</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>" enctype="multipart/form-data">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Kategori</label>
				<div class="col-sm-8">
				<input type="text" class="form-control" name="nama" required placeholder="Kategori" value="<?php echo $data->nama; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"></label>
				<div class="col-sm-8">
					<div class="form-submit" style="text-align: left !important;">
						<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
					</div>
				</div>
			</div>
		</div>
		
	</form>
</div>