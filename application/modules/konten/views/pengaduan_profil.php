
	<script type="text/javascript" src="<?php echo base_url()?>fancyBox/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>fancyBox/source/jquery.fancybox.css" media="screen" />

	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();		
		});
	</script>

	<style type="text/css" media="screen">
        .foto{
            width: 100%;
            height: 200px;
        }
    </style>

<div id="header" class="container-fluid">
	<h1 class="col-md-6"><img src="<?php echo base_url(); ?>img/pengaduan.png">&nbsp; <?php echo $data->judul; ?></h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-3">
		<dl class="profile">
			<dt>Tanggal Posting</dt>
			<dd><?php echo $data->created_at; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Kategori</dt>
			<dd><?php echo $data->kategori; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Status</dt>
			<dd><?php echo status_pengaduan($data->status); ?></dd>
		</dl>
		<div class="separator2"></div>
	</div>
	<div class="col-sm-9">
	 	<div class="clearfix"></div>
		<?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
		<div id="pengaduan-map" style="height: 250px;"></div>
		<?php else: ?>
		<div class="blank-map">Belum ada lokasi</div>
		<?php endif; ?>

		<div class="row">
            <br>
        </div>

        <?php if ($pict->num_rows() == 0): ?>
		<div class="alert alert-warning">Belum ada foto.</div>
		<?php else: ?>
		<?php foreach ($pict->result() as $res): ?>
				<div class="col-md-3">
					<a class="fancybox fancybox.image" data-fancybox-group="data" href="<?php echo base_url("/img/pengaduan/{$res->id_pengaduan}/{$res->nama_file}"); ?>">
						<img src="<?php echo base_url("/img/pengaduan/{$res->id_pengaduan}/{$res->nama_file}"); ?>" class="img-responsive foto">
					</a>
				</div>
        	<?php endforeach; ?>
		<?php endif; ?>
		<div class="clearfix"></div>
		<div class="separator"></div>

		<?php echo str_replace("\n","<br>",$data->keterangan); ?>
	</div>
</div>

<?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
<?php $position = location($data->lokasi); ?>
<script>
function initMap() {
	var pengaduan = { lat: <?php echo $position['lat'] ?>, lng: <?php echo $position['long'] ?> };
	var map = new google.maps.Map(document.getElementById('pengaduan-map'), { zoom: 15, center: pengaduan });
	var marker = new google.maps.Marker({ position: pengaduan, map: map, icon: '<?php echo base_url('/img/marker.png'); ?>' });
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnoCzqdU2bCgUMFH8asKC3YgrlYNY4KKM&callback=initMap"></script>
<?php endif; ?>