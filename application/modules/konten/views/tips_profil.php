<script type="text/javascript" src="<?php echo base_url()?>fancyBox/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>fancyBox/source/jquery.fancybox.css" media="screen" />

<script type="text/javascript">
	$(document).ready(function() {
		$('.fancybox').fancybox();		
	});
</script>
<style type="text/css" media="screen">
        .foto{
            width: 100%;
            height: 200px;
        }
 </style>
<div id="header" class="container-fluid">
	<h1 class="col-md-6"><img src="<?php echo base_url(); ?>img/tips.png">&nbsp; <?php echo $data->judul; ?></h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-2">
		<dl class="profile">
			<dt>Tanggal Publish</dt>
			<dd><?php echo $data->tgl_publish; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Status</dt>
			<dd><?php echo aktif_pasif($data->status); ?></dd>
		</dl>
	<div class="separator2"></div>
	</div>
	<div class="col-sm-9">
		<?php if (($data->nama_file=='' && $data->jenis_media==0) || ($data->url=='' && $data->jenis_media==1)): ?>
		<div class="alert alert-warning">Belum ada foto.</div>
		<?php else: ?>
		<div class="col-sm-7">
			<?php if($data->jenis_media==0): ?>
			<a class="fancybox fancybox.image" data-fancybox-group="data" href="<?php echo base_url("/img/tips/{$data->id}/{$data->nama_file}"); ?>">
			<img src="<?php echo base_url("/img/tips/{$data->id}/{$data->nama_file}"); ?>" class="img-responsive foto">
			</a>
			<?php else: ?>
			<a class="fancybox fancybox.iframe" data-fancybox-group="data" href="<?php echo $res->url;?>" >
			<iframe src="<?php echo $data->url;?>" class="img-responsive foto"></iframe>
			</a>
			<?php endif; ?>
		</div>
		<?php endif; ?>
		<div class="clearfix"></div>
		<div class="separator2"></div>
		<div class="col-sm-12">
		<?php echo str_replace("\n","<br>",$data->artikel); ?>
		</div>
	</div>
</div>