<link rel="stylesheet" href="<?php echo base_url()?>js/zebra_datepicker/public/css/default.css" type="text/css">  

<script type="text/javascript" src="<?php echo base_url()?>js/zebra_datepicker/public/javascript/zebra_datepicker.src.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.form.min.js"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Tips & Trik</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>" enctype="multipart/form-data">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<input type="hidden" id="urlorg" value="<?php echo $data->url; ?>">
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Judul</label>
				<div class="col-sm-9">
				<input type="text" class="form-control" name="judul" placeholder="Judul" value="<?php echo $data->judul; ?>">	
				</div>
			</div>
		</div>
		<!--<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Tgl. Publish</label>
				<div class="col-sm-9">
				<input type="text" class="form-control" id="tgl_publish" name="tgl_publish" value="<?php echo $data->tgl_publish==null?date('Y-m-d'):$data->tgl_publish; ?>">
				</div>
			</div>
		</div>-->
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">Status</label>
				<div class="col-sm-8">
					<label><input type="radio" name="status" value="1" <?php if($data->status == '1'){echo 'checked';}else{echo '';}?> checked/> AKTIF</label>
					<label><input type="radio" name="status" value="0" <?php if($data->status == '0'){echo 'checked';}else{echo '';}?>/> NON AKTIF</label>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">Artikel</label>
				<div class="col-sm-9">
					<textarea class="form-control" name="artikel" placeholder="Artikel" style="height:300px;"><?php echo $data->artikel; ?></textarea>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">Media</label>
				<div class="col-sm-9">
					<select class="form-control" id="jenis_media" name="jenis_media" style="width: 85px;" onchange="get_media();">
						<?php 
						$foto='';
						$video='';
						if($data->jenis_media==0){
							$foto='selected';
						}elseif ($data->jenis_media==1) {
							$video='selected';
						}
						?>
						<option value="0" <?php echo $foto;?> >Gambar</option>
						<option value="1" <?php echo $video;?>>Video</option>
					</select>
					<div class="media"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"></label>
				<div class="form-submit col-md-8" style="text-align: left !important;">
				<button type="submit" class="btn btn-success" ><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
$().ready(function() {
	get_media();
});
$('#tgl_publish').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});

function get_media() {
	var media = $("#jenis_media").val();
	var urlorg = $("#urlorg").val();
	// alert(media);

	if(media==1){
		$opt='<input type="text" class="form-control" name="url" placeholder="Url Media" value="'+urlorg+'">	';
	}else{
		$opt='<input type="file" name="foto">';
	}
		$(".media").html($opt);
}
</script>