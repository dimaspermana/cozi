<link rel="stylesheet" href="<?php echo base_url()?>js/zebra_datepicker/public/css/default.css" type="text/css">  
<link rel="stylesheet" href="<?php echo base_url()?>css/magic-check.css" type="text/css">  

<script type="text/javascript" src="<?php echo base_url()?>js/zebra_datepicker/public/javascript/zebra_datepicker.src.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.form.min.js"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Pengaduan</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>" enctype="multipart/form-data">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Judul</label>
				<div class="col-sm-8">
				<input type="text" class="form-control" name="judul" required placeholder="Judul" value="<?php echo $data->judul; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Kategori</label>
				<div class="col-sm-8">
				<select class="form-control required" name="id_kategori">
					<?php echo modules::run('options/kategori', $data->id_kategori); ?>
				</select>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">Lokasi (Koordinat)</label>
				<div class="col-sm-8">
				<input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="Lokasi (Koordinat)" value="<?php echo $data->lokasi; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">Keterangan</label>
				<div class="col-sm-8">
					<textarea class="form-control" name="keterangan" placeholder="Keterangan" style="height: 200px;"><?php echo $data->keterangan; ?></textarea>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">File Upload</label>
				<div class="col-sm-10">
					<div class="row">
						<div class="col-md-2">
							<span class="btn btn-primary fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>Add files</span>
							</span>
						</div>
					</div>
					<!-- <span class="btn btn-primary fileinput-button">
					<i class="glyphicon glyphicon-plus"></i>
					<span>Add files</span>
					</span> -->
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"> </label>
				<div class="col-sm-8 up-satu">
					
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"></label>
				<div class="col-sm-8">
					<div class="form-submit" style="text-align: left !important;">
						<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
					</div>
				</div>
			</div>
		</div>
		
	</form>
</div>
<script>
$('#tgl_posting').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});

$(".fileinput-button").click(function(){
	var tr = "";
	var check = $("input[name=media]:checked").val();

	tr = '<p><input type="file" name="filename[]" value=""></p>';

	$(".up-satu").append(tr);
});
</script>