<link rel="stylesheet" href="<?php echo base_url()?>js/zebra_datepicker/public/css/default.css" type="text/css">  
<link rel="stylesheet" href="<?php echo base_url()?>css/magic-check.css" type="text/css">  

<script type="text/javascript" src="<?php echo base_url()?>js/zebra_datepicker/public/javascript/zebra_datepicker.src.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.form.min.js"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Sosialisasi</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>" enctype="multipart/form-data">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Kegiatan</label>
				<div class="col-sm-8">
				<input type="text" class="form-control" name="kegiatan" required placeholder="Kegiatan" value="<?php echo $data->kegiatan; ?>">	
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Tgl. Sosialisasi</label>
				<div class="col-sm-8">
				<input type="text" class="form-control" id="tgl_sosialisasi" name="tgl_sosialisasi" value="<?php echo $data->tgl_sosialisasi==null?date('Y-m-d'):$data->tgl_sosialisasi; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Kota</label>
				<div class="col-sm-8">
					<select name="id_kota" class="form-control required">
						<option value="">- Pilih Kota -</option>
						<?php echo modules::run('options/kota', $data->id_kota); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"><span class="red">*</span>Tempat</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="tempat" required placeholder="Tempat" value="<?php echo $data->tempat; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">Artikel</label>
				<div class="col-sm-8">
					<textarea class="form-control" name="artikel" placeholder="Artikel" style="height: 200px;"><?php echo $data->artikel; ?></textarea>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2">File Upload</label>
				<div class="col-sm-10">
					<div class="row">
						<div class="col-md-2">
							<span class="btn btn-primary fileinput-button">
							<i class="glyphicon glyphicon-plus"></i>
							<span>Add files</span>
							</span>
						</div>
						<div class="col-md-10">
							<label class="radio-inline">
							  <input type="radio" name="media" id="inlineRadio1" value="0" class="magic-radio" checked>
							  <label for="inlineRadio1">
								  Gambar
							  </label>
							</label>
							<label class="radio-inline">
							  <input type="radio" name="media" id="inlineRadio2" value="1" class="magic-radio">
							  <label for="inlineRadio2">
								  Video
							  </label>
							</label>
						</div>
					</div>
					<!-- <span class="btn btn-primary fileinput-button">
					<i class="glyphicon glyphicon-plus"></i>
					<span>Add files</span>
					</span> -->
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"> </label>
				<div class="col-sm-8 up-satu">
					
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-md-8">
			<div class="form-group">
				<label class="control-label col-sm-2"></label>
				<div class="col-sm-8">
					<div class="form-submit" style="text-align: left !important;">
						<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
					</div>
				</div>
			</div>
		</div>
		
	</form>
</div>
<script>
$('#tgl_publish').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});

$('#tgl_sosialisasi').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});

$(".fileinput-button").click(function(){
	var tr = "";
	var check = $("input[name=media]:checked").val();
	if(check=="0"){
		tr = '<p><input type="file" name="filename[]" value=""></p>';
	}else{
		tr = '<p><input type="text" class="form-control" name="videosrc[]" placeholder="Url Media"></p>';
	}
	
	$(".up-satu").append(tr);
});
</script>