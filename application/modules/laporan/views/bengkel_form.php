<link rel="stylesheet" href="<?php echo base_url()?>js/zebra_datepicker/public/css/default.css" type="text/css">  

<script type="text/javascript" src="<?php echo base_url()?>js/zebra_datepicker/public/javascript/zebra_datepicker.src.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.form.min.js"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Laporan Harian Bengkel / Workshop</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-12">
			<div class="form-group">
				<label class="control-label col-sm-4"><span class="red">*</span> Bengkel / Workshop</label>
				<div class="col-sm-6">
					<select class="form-control required" name="id_bengkel">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih Bengkel / Workshop -</option>
						<?php endif; ?>
						<?php echo modules::run('options/bengkel', $data->id_bengkel); ?>
					</select>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		
		<div class="separator"></div>
		<div class="col-md-6">
		
			<div class="form-group">
				<label class="control-label col-sm-5">Tgl Service</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="tgl_laporan" name="tgl_laporan" value="<?php echo $data->tgl_laporan==null?date('Y-m-d'):$data->tgl_laporan; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span>Nama</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span>Alamat</label>
				<div class="col-sm-7">
					<textarea class="form-control required" name="alamat" placeholder="Alamat"><?php echo $data->alamat; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">No. Telp</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="no_telp" value="<?php echo $data->no_telp; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Lokasi</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="lokasi" value="<?php echo $data->lokasi; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span>Jenis Service</label>
				<div class="col-sm-7">
					<select class="form-control required" name="jenis_servis" id="jenis_servis">
						<option value="">- Pilih Jenis Service-</option>
						<option value="CUCI">CUCI</option>
						<option value="TAMBAH FREON">TAMBAH FREON</option>
						<option value="VAKUM">VAKUM</option>
						<option value="PENGGANTIAN PART">PENGGANTIAN PART</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Tgl Service Berikutnya</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="tgl_servis_berikutnya" name="tgl_servis_berikutnya" value="<?php echo $data->tgl_servis_berikutnya==null?date('Y-m-d'):$data->tgl_servis_berikutnya; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			
			<div class="form-group">
				<label class="control-label col-sm-5">Nomor AC</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="no_ac" value="<?php echo $data->no_ac; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span>Merk AC</label>
				<div class="col-sm-7">
					<select class="form-control required" name="merk_ac" id="merk_ac">
						<option value="">- Pilih Merk -</option>
						<option value="DAIKIN"> DAIKIN </option>
						<option value="MITSUBISHI"> MITSUBISHI </option>
						<option value="PANASONIC"> PANASONIC </option>
						<option value="LG"> LG </option>
						<option value="MIDEA"> MIDEA </option>
						<option value="HAIER"> HAIER </option>
						<option value="SAMSUNG"> SAMSUNG </option>
						<option value="ELECTROLUX"> ELECTROLUX </option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">PK</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="pk" value="<?php echo $data->pk; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span>Jenis</label>
				<div class="col-sm-7">
					<select class="form-control required" name="jenis" id="jenis">
						<option value="">- Pilih Jenis -</option>
						<option value="R-22"> R-22 </option>
						<option value="R-123"> R-123 </option>
						<option value="R-134A"> R-134A </option>
						<option value="R-410A"> R-410A </option>
						<option value="R-404A"> R-404A </option>
						<option value="R-407C"> R-407C </option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Cek Ampere(A)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="ampere" value="<?php echo $data->ampere; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Cek Pressure(psi)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="pressure" value="<?php echo $data->pressure; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Biaya</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="biaya" value="<?php echo $data->biaya; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>
<script>
$('#tgl_laporan').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});
$('#tgl_servis_berikutnya').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});
$('#merk_ac option[value="<?php echo $data->merk_ac;?>"]').attr("selected",true);
$('#jenis option[value="<?php echo $data->jenis;?>"]').attr("selected",true);
$('#jenis_servis option[value="<?php echo $data->jenis_servis;?>"]').attr("selected",true);
</script>