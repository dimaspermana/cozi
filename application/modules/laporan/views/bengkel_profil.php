
	<script type="text/javascript" src="<?php echo base_url()?>fancyBox/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>fancyBox/source/jquery.fancybox.css" media="screen" />

	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();		
		});
	</script>

	<style type="text/css" media="screen">
        .foto{
            width: 100%;
            height: 200px;
        }
    </style>

<div id="header" class="container-fluid">
	<h1 class="col-md-6">Laporan Harian <?php echo $data->bengkel; ?></h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label col-sm-5">Tanggal Service</label>
			<label class="control-label col-sm-7"><?php echo $data->tgl_laporan; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Nama Customer</label>
			<label class="control-label col-sm-7"><?php echo $data->nama; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Alamat</label>
			<label class="control-label col-sm-7"><?php echo $data->alamat; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">No. Telp</label>
			<label class="control-label col-sm-7"><?php echo $data->no_telp; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Jenis Service</label>
			<label class="control-label col-sm-7"><?php echo $data->jenis_servis; ?></label>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="form-group">
			<label class="control-label col-sm-5">Tgl Service Berikutnya</label>
			<label class="control-label col-sm-7"><?php echo $data->tgl_servis_berikutnya; ?></label>
		</div>
		<div class="clearfix"></div>
		<div class="form-group">
			<label class="control-label col-sm-5">Nomor AC</label>
			<label class="control-label col-sm-7"><?php echo $data->no_ac; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Merk AC</label>
			<label class="control-label col-sm-7"><?php echo $data->merk_ac; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">PK</label>
			<label class="control-label col-sm-7"><?php echo $data->pk; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Jenis</label>
			<label class="control-label col-sm-7"><?php echo $data->jenis; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Cek Ampere</label>
			<label class="control-label col-sm-7"><?php echo $data->ampere; ?></label>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Cek Pressure</label>
			<label class="control-label col-sm-7"><?php echo $data->pressure; ?></label>
		</div>

	</div>
	<div class="col-sm-8">
	 	<div class="clearfix"></div>
		<?php if ($data->lokasi != NULL && $data->lokasi != '' && $data->lokasi != '0'): ?>
		<div id="pengaduan-map" style="height: 250px;"></div>
		<?php else: ?>
		<div class="blank-map">Belum ada lokasi</div>
		<?php endif; ?>
		<div class="clearfix"></div>
	</div>
	<!-- <div class="col-sm-3">
	 	<div class="clearfix"></div>
		<?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
		<div id="pengaduan-map" style="height: 250px;"></div>
		<?php else: ?>
		<div class="blank-map">Belum ada lokasi</div>
		<?php endif; ?>
		<div class="clearfix"></div>
		<div class="separator"></div>
	</div>-->
</div>

<?php if ($data->lokasi != NULL && $data->lokasi != '' && $data->lokasi != '0'): ?>
<?php $position = location($data->lokasi); ?>
<script>
function initMap() {
	var pengaduan = { lat: <?php echo $position['lat'] ?>, lng: <?php echo $position['long'] ?> };
	var map = new google.maps.Map(document.getElementById('pengaduan-map'), { zoom: 15, center: pengaduan });
	var marker = new google.maps.Marker({ position: pengaduan, map: map, icon: '<?php echo base_url('/img/marker.png'); ?>' });
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnoCzqdU2bCgUMFH8asKC3YgrlYNY4KKM&callback=initMap"></script>
<?php endif; ?>