<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bengkel_bulanan extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('bengkel_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->bengkel_model->setBulan($q_encoded);
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
				'created_at'=> array('text' => 'Waktu Input'),
				'bengkel' 	=> array('text' => 'Nama Bengkel'),
				'nama' 		=> array('text' => 'Nama Customer'),
				'tgl_laporan' 		=> array('text' => 'Tgl. Service'),
				'merk_ac' 		=> array('text' => 'Merk AC'),
				'pengguna'	=> array('text' => 'Pengguna Input'),
			),
			'num_rows' => $this->bengkel_model->getBulanan()->num_rows(),
			'item' => 'bengkel',
			'order' => 'desc',
			'warning' => 'bengkel',
			'checkbox' => FALSE,
		));
		
		$total_preasure = 0;
		$total_biaya = 0;
		$pembagi = 100;
		foreach($this->bengkel_model->getBulanan()->result() AS $dt) {
			$total_preasure += $dt->pressure;
			$total_biaya += $dt->biaya;
		}
		
		$this->bengkel_model->setLimitBulanan($this->grid->params());
		$this->grid->source($this->bengkel_model->getBulanan());
		$this->grid->disable_all_acts();
		
		$this->page->view('bengkel_bulanan_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'bulan' => $this->bengkel_model->bulan,
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->bengkel_model->filter,
			'keyword' => $this->bengkel_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
			'total_freon' => $total_preasure / $pembagi,
			'total_biaya' => $total_biaya,
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode( $this->input->post() ));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('bengkel_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->bengkel_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('id_bengkel', 'tgl_laporan', 'upper_nama', 'upper_alamat', 'upper_no_ac', 'upper_merk_ac', 'pk', 'upper_jenis', 'ampere', 'pressure', 'upper_jenis_servis', 'tgl_servis_berikutnya', 'biaya');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$data = $this->form_data();
		db_insert('bengkel_laphar', $data);
		redirect($this->input->post('redirect'));
	}
	
	
	public function update($id)
	{
		$data = $this->form_data();
		db_update('bengkel_laphar', $data, array('id' => $id));
		redirect($this->input->post('redirect'));
	}
	
	
	public function profil($id)
	{
		$this->page->view('bengkel_profil', array (
			'data' => $this->bengkel_model->by_id($id),
		));
	}
	
	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('bengkel_laphar', array('id' => $id));
		redirect($this->agent->referrer());
	}
	
}

/* End of file bengkel.php */
/* Location: ./application/modules/hpmp/controllers/bengkel.php */