<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Data Foam - Perusahaan Besar</h1>
	<div class="col-sm-4 col-xs-12 pull-right" id="top-search">
		<form method="post" action="<?php echo $show_url; ?>">
			<div class="input-group col-md-12">
				<input type="text" class="form-control" placeholder="Cari Nama Perusahaan Besar..." name="keyword" value="<?php echo $keyword; ?>">
				<div class="input-group-btn">
					<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
		</form>
	</div>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-12">
		<div id="map-awal-bengkel" style="position: relative; overflow: hidden;height:395px;">
		</div>
	</div>
	<div class="clearfix"></div>
	<br>
	<?php if (user_session('tingkatan')!=4): ?>
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $add_url; ?>" class="btn btn-success" title="Tambah Data"><i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs">Tambah Data</span></a>
			</div>
			<?php if(user_session('tingkatan') == '1'){?>
			<select class="col-sm-2 col-md-2 propinsi_id" name="id_propinsi">
				<?//php if(user_session('tingkatan') != '2'){?>
					<option value="">SEMUA PROVINSI</option>
					<?php //}else{
						
					//}?>
				<?php echo modules::run('options/provinsi', $filter->id_propinsi); ?>
			</select>
			<?php }else if(user_session('tingkatan') == '2'){
						?>
			<select class="col-sm-2 col-md-2 kota_id" name="id_kota">
				
					<option value="">SEMUA KOTA</option>
				
				<?php echo modules::run('options/kotaku', $filter->id_kota); ?>
			</select>
			<?php }else{
						
					}?>
			<button class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i></button>
			<div class="clearfix"></div>
		</div>
	</form>
	<?php endif;?>
	<div id="num-data"><i class="glyphicon glyphicon-stats"></i>&nbsp; <?php echo $total_data; ?> data</div>
	<?php echo $grid; ?>
	<?php echo $pagelink; ?>
</div>
<script>

	<?php

	if (user_session('grup_pengguna') == 'balai') {
		$lat = -2.952997;
		$lng = 117.521710;
		$zoom = 5;
	}
	else if ($filter->id_propinsi == '') {
		$lat = -2.952997;
		$lng = 117.521710;
		$zoom = 5;
	}
	else if ($filter->id_kota == '') {
		$center = $this->db->select('center')->get_where('propinsi', array('id' => $filter->id_propinsi))->row('center');
		$location = location($center);
		$lat = $location['lat'];
		$lng = $location['long'];
		$zoom = 10;
	}
	else {
		$this->db->select("kota.*, b.center");
		$this->db->join("propinsi AS b", "kota.id_propinsi = b.id", 'left');
		$this->db->where(array('kota.id' => $filter->id_kota));
		$center = $this->db->get('kota')->row('center');

		//$center = $this->db->select('center')->get_where('propinsi', array('id' => $filter->id_propinsi))->row('center');
		$location = location($center);
		$lat = $location['lat'];
		$lng = $location['long'];
		$zoom = 10;
	}

	?>
	function initMap() {
		var uluru = {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>};
		var map = new google.maps.Map(document.getElementById('map-awal-bengkel'), { zoom: <?php echo $zoom; ?>,center: uluru});
		var markers = [];
		var pesan = "";
		$.getJSON("<?php echo $this->page->base_url("get_besar")?>", {}, function(res){

			if(res.length != 0){
				$.each(res, function (i, result) {
					var lokasi = result.lokasi.split(",");
					// var myLatlng = new google.maps.LatLng(lokasi[0],lokasi[1]);
					//             var marker = new google.maps.Marker({
					//                 position: myLatlng,
					//                 map: map,
					//                 icon: '<?php echo base_url('/img/marker.png'); ?>',
					//                 title: result.nama
					//             });
					var latLng = new google.maps.LatLng(lokasi[0],lokasi[1]);
					var marker = new google.maps.Marker({'position': latLng,icon:'<?php echo base_url('/img/marker.png'); ?>'});

					var contentString = '<div id="content">'+
						'<div id="siteNotice">'+
						'</div>'+
						'<div id="bodyContent">'+
						// '<div class="divkiri">'+
						'<h5>'+result.nama+'</h5>'+
						'<a target="_blank" href="https://maps.google.com/?daddr='+lokasi[0]+','+lokasi[1]+'"><p> NAVIGASI</p></a>'+
						// '<p style="font-size:9px;">'+lokasi[0]+' , '+lokasi[1]+'</p>'+
						// '</div>'+
						// 		'<div class="divkanan">'+
						// 	// 		'<div class="outer">'+
						// 	// '<div class="middle">'+
						// 	// 	'<div class="inner">'+
						// 		'<a target="_blank" href="https://maps.google.com/?daddr='+lokasi[0]+','+lokasi[1]+'"><h3><img src="<?php echo base_url('/img/direction.png'); ?>" alt=""></h3></a>'+
						// // 		'</div>'+
						// // 	'</div>'+
						// // '</div>'+
						// 		'</div>'+
						'</div>'+
						'</div>';


					var infowindow = new google.maps.InfoWindow({
						content: contentString,
					});

					marker.addListener('click', function () {
						infowindow.open(map, marker);
					});

					markers.push(marker);



					// pesan.push(contentString);
				});
				var markerCluster = new MarkerClusterer(map, markers, {imagePath: "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m"});
			}

		});
		// console.log(markers);


	}


</script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnoCzqdU2bCgUMFH8asKC3YgrlYNY4KKM&callback=initMap"></script>