    <style type="text/css" media="screen">
        .foto{
            width: 100%;
            height: 200px;
        }
    </style>
<div id="header" class="container-fluid">
    <h1 class="col-md-6"><img src="<?php echo base_url(); ?>img/ac.png">&nbsp; <?php echo $data->nama; ?></h1>
    <div class="col-md-6">
        <div class="btn-group pull-right">
            <a href="<?php echo $form_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs">UBAH</span></a>
            <a href="<?php echo $images_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">UPLOAD FOTO</span></a>
        </div>
    </div>
</div>
<div id="main-container" class="container-fluid">
    <div class="col-sm-3">
        <dl class="profile">
            <dt>Alamat</dt>
            <dd><?php echo $data->alamat; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Kota</dt>
            <dd><?php echo $data->kota; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Tanggal Berlaku</dt>
            <dd><?php echo $data->tgl_berlaku; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Profil</dt>
            <dd><?php echo $data->profil; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Peralatan RAC</dt>
            <dd><?php echo $data->peralatan_rac; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Peralatan Bantuan</dt>
            <dd><?php echo $data->peralatan_bantuan; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Jenis Pelatihan</dt>
            <dd><?php echo $data->jenis_pelatihan; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Nama Kontak</dt>
            <dd><?php echo $data->nama_kontak; ?></dd>
        </dl>
        <dl class="profile">
            <dt>No. Telp.</dt>
            <dd><?php echo $data->no_telp; ?></dd>
        </dl>
        <dl class="profile">
            <dt>Email</dt>
            <dd><?php echo $data->email; ?></dd>
        </dl>
        <div class="separator2"></div>
    </div>
    <div class="col-sm-9">
        <div class="clearfix"></div>
        <?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
            <div id="institusi-map" style="height: 250px;"></div>
        <?php else: ?>
            <div class="blank-map">Belum ada lokasi</div>
        <?php endif; ?>
    </div>
    <div class="col-sm-9">
        <div class="row">
            <br>
        </div>
    </div>
    <div class="col-sm-9">

        <?php if ($images->num_rows() == 0): ?>
            <div class="alert alert-warning">Belum ada foto.</div>
        <?php else: ?>
            <?php foreach ($images->result() as $img): ?>
                <div class="col-sm-4">
                    <img src="<?php echo base_url("/img/institusi/{$img->id_institusi}/{$img->nama_file}"); ?>" class="img-responsive foto">
                    <div class="caption"><?php echo $img->keterangan; ?></div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
        <div class="clearfix"></div>
    </div>
</div>
<?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
    <?php $position = location($data->lokasi); ?>
    <script>
        function initMap() {
            var institusi = { lat: <?php echo $position['lat'] ?>, lng: <?php echo $position['long'] ?> };
            var map = new google.maps.Map(document.getElementById('institusi-map'), { zoom: 15, center: institusi });
            var marker = new google.maps.Marker({ position: institusi, map: map, icon: '<?php echo base_url('/img/marker.png'); ?>' });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnoCzqdU2bCgUMFH8asKC3YgrlYNY4KKM&callback=initMap"></script>
<?php endif; ?>