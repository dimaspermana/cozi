<style type="text/css" media="screen">
    .foto{
        width: 100%;
        height: 200px;
    }
</style>	
<div id="header" class="container-fluid">
	<h1 class="col-md-6"><img src="<?php echo base_url(); ?>img/ac.png">&nbsp; <?php echo $data->nama; ?></h1>
	<div class="col-md-6">
		<div class="btn-group pull-right">
			<a href="<?php echo $form_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs">UBAH</span></a>
			<a href="<?php echo $images_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">UPLOAD FOTO</span></a>
		</div>
	</div>
</div>
<div id="main-container" class="container-fluid">
	<div class="col-sm-3">
		<dl class="profile">
			<dt>Jenis Usaha</dt>
			<dd><?php echo jenis_usaha($data->jenis_usaha); ?></dd>
		</dl>
		<dl class="profile">
			<dt>Alamat</dt>
			<dd><?php echo $data->alamat; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Kota</dt>
			<dd><?php echo $data->kota; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Jenis Izin Usaha</dt>
			<dd><?php echo $data->izin_usaha; ?></dd>
		</dl>
		<dl class="profile">
			<dt>No. Registrasi</dt>
			<dd><?php echo $data->no_registrasi; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Keanggotaan Asosiasi</dt>
			<dd><?php echo $data->keanggotaan_asosiasi; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Pemilik</dt>
			<dd><?php echo $data->pemilik; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Penanggung Jawab</dt>
			<dd><?php echo $data->penanggung_jawab; ?></dd>
		</dl>
		<dl class="profile">
			<dt>No. Telp.</dt>
			<dd><?php echo $data->no_telp; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Email</dt>
			<dd><?php echo $data->email; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Peralatan yang Dimiliki</dt>
			<dd><?php echo $data->peralatan; ?></dd>
		</dl>
		<dl class="profile">
			<dt>Teknisi Bersetifikat BNSP</dt>
			<dd><?php echo $data->teknisi_bnsp; ?> orang</dd>
		</dl>
	</div>
	<div class="col-sm-9">
		<?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
		<div id="bengkel-map"></div>
		<?php else: ?>
		<div class="blank-map">Belum ada lokasi</div>
		<?php endif; ?>
		
		<!--
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th rowspan="2">Aktifitas</th>
						<th colspan="12">Bulan</th>
					</tr>
					<tr>
						<?php for ($i = 1; $i <= 12; $i++): ?>
						<?php $highlight = ($i == date('m')) ? 'class="highlight"' : ''; ?>
						<th <?php echo $highlight; ?> ><?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ?></th>
						<?php endfor; ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($hpmp as $row): ?>
					<tr>
						<td><?php echo $row['text']; ?></td>
						<?php foreach ($row['value'] as $key => $val): ?>
						<?php $highlight = ($key+1 == date('m')) ? 'class="highlight"' : ''; ?>
						<td <?php echo $highlight; ?> align="right"><?php echo $val; ?></td>
						<?php endforeach; ?>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		-->
		
		<?php if ($images->num_rows() == 0): ?>
		<div class="alert alert-warning">Belum ada foto.</div>
		<?php else: ?>
		<?php foreach ($images->result() as $img): ?>
		<div class="col-sm-4">
			<img src="<?php echo base_url("/img/bengkel/{$img->id_bengkel}/{$img->nama_file}"); ?>" class="img-responsive foto">
			<div class="caption"><?php echo $img->keterangan; ?></div>
		</div>
		<?php endforeach; ?>
		<?php endif; ?>
		<div class="clearfix"></div>
	</div>
</div>

<?php if ($data->lokasi != NULL && $data->lokasi != ''): ?>
<?php $position = location($data->lokasi); ?>
<script>
function initMap() {
	var bengkel = { lat: <?php echo $position['lat'] ?>, lng: <?php echo $position['long'] ?> };
	var map = new google.maps.Map(document.getElementById('bengkel-map'), { zoom: 15, center: bengkel });
	var marker = new google.maps.Marker({ position: bengkel, map: map, icon: '<?php echo base_url('/img/marker.png'); ?>' });
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnoCzqdU2bCgUMFH8asKC3YgrlYNY4KKM&callback=initMap"></script>
<?php endif; ?>