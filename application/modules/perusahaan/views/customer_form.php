<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Form Foam - Customer Supplier</h1>
</div>
<div id="main-container" class="container-fluid">
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-9">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Supplier</label>
				<div class="col-sm-7">
					<select class="form-control required" name="id_supplier">
						<?php if (user_session('tingkatan') != 4): ?>
						<option value="">- Pilih System House -</option>
						<?php endif; ?>
						<?php echo modules::run('options/supplier_', $data->id_supplier); ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Customer</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama" placeholder="Nama Customer" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Alamat</label>
				<div class="col-sm-7">
					<textarea class="form-control required" name="alamat" placeholder="Alamat"><?php echo $data->alamat; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> No. Telp.</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="no_telp" placeholder="No. Telp." value="<?php echo $data->no_telp; ?>">
				</div>
			</div>
			
		</div>
		
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>