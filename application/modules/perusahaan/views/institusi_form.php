<link rel="stylesheet" href="<?php echo base_url()?>js/zebra_datepicker/public/css/default.css" type="text/css">  

<script type="text/javascript" src="<?php echo base_url()?>js/zebra_datepicker/public/javascript/zebra_datepicker.src.js"></script>
<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Form Institusi Pendidikan</h1>
</div>
<div id="main-container" class="container-fluid">
	<?php if ($data->nama != ''): ?>
		<div class="actions">
			<div class="btn-group pull-left">
				<a href="<?php echo $form_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-file"></i> <span class="hidden-xs">FORM</span></a>
				<a href="<?php echo $images_url; ?>" class="btn btn-default"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">FOTO</span></a>
			</div>
			<div class="clearfix"></div>
		</div>
	<?php endif; ?>
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Institusi Pendidikan</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama" placeholder="Nama Institusi Pendidikan" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span>Alamat</label>
				<div class="col-sm-7">
					<textarea class="form-control required" name="alamat" placeholder="Alamat"><?php echo $data->alamat; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span>Kota</label>
				<div class="col-sm-7">
				<select name="id_kota" class="form-control required">
						<option value="">- Pilih Kota -</option>
						<?php echo modules::run('options/kota', $data->id_kota); ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">TUK</label>
				<div class="col-sm-1">
					<input type="checkbox" class="form-control" name="tuk" id="tuk" value="1" <?php if ($data->tuk == '1') echo "checked"; ?> onclick="get_tanggal(this);">
					<input type="hidden" id="tuks" value=<?php echo $data->tuk; ?>>
				</div>
			</div>
			<div class="form-group" id="tanggal">
				<label class="control-label col-sm-5">Tgl. Izin Berlaku</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" id="tgl_berlaku" name="tgl_berlaku" value="<?php echo $data->tgl_berlaku==null?date('Y-m-d'):$data->tgl_berlaku; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5">Profil Institusi</label>
				<div class="col-sm-7">
					<textarea class="form-control" name="profil" placeholder="Profil Institusi"><?php echo $data->profil; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Alat Pelatihan RAC</label>
				<div class="col-sm-7">
					<textarea class="form-control" name="peralatan_rac" placeholder="Alat Pelatihan RAC"><?php echo $data->peralatan_rac; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Jenis Pelatihan</label>
				<div class="col-sm-7">
					<textarea class="form-control" name="jenis_pelatihan" placeholder="Jenis Pelatihan"><?php echo $data->jenis_pelatihan; ?></textarea>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Kontak</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama_kontak" placeholder="Nama Kontak" value="<?php echo $data->nama_kontak; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> No. Telp.</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="no_telp" placeholder="No. Telp." value="<?php echo $data->no_telp; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Email</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $data->email; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Lokasi</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="lokasi" placeholder="Lokasi" value="<?php echo $data->lokasi; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>
<script>
$().ready(function() {
$('#tgl_berlaku').Zebra_DatePicker({
		offset : [-300,100],
		format : 'Y-m-d'
	});
if($('#tuks').val()==1){
	$("#tanggal").show();
}else{
	$("#tanggal").hide();
}
});

function get_tanggal(obj) {
    if(obj.checked) {
		$("#tanggal").show();
	}else{
		$("#tanggal").hide();
	}
};
</script>