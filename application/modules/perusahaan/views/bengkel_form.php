<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Form Bengkel / Workshop</h1>
</div>
<div id="main-container" class="container-fluid">
	<?php if ($data->nama != ''): ?>
	<div class="actions">
		<div class="btn-group pull-left">
			<a href="<?php echo $form_url; ?>" class="btn btn-success"><i class="glyphicon glyphicon-file"></i> <span class="hidden-xs">FORM</span></a>
			<a href="<?php echo $images_url; ?>" class="btn btn-default"><i class="glyphicon glyphicon-picture"></i> <span class="hidden-xs">FOTO</span></a>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php endif; ?>
	<form class="form-horizontal validate-form" method="post" action="<?php echo $action_url; ?>">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Bengkel / Workshop</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="nama" placeholder="Nama Bengkel / Workshop" value="<?php echo $data->nama; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Jenis Usaha</label>
				<div class="col-sm-7">
				<select class="form-control required" name="jenis_usaha">
					<?php 
						$jenis_1="";$jenis_2="";$jenis_3="";$jenis_4="";
						for($i=1;$i<=4;$i++){
							if($data->jenis_usaha==$i){
							$var = 'jenis_'.$i;
							$$var = 'selected';
							}
						}
					?>
						<option value="">Pilih Jenis Usaha</option>
						<option value="1" <?php echo $jenis_1;?>>JASA SERVICE AC PANGGILAN</option>
						<option value="2" <?php echo $jenis_2;?>>INSTALLER</option>
						<option value="3" <?php echo $jenis_3;?>>SERVICE COLD STORAGE</option>
						<option value="4" <?php echo $jenis_4;?>>SERVICE MANDIRI (GEDUNG / HOTEL)</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Deskripsi Usaha</label>
				<div class="col-sm-7">
					<textarea class="form-control required" name="deskripsi_usaha" placeholder="Deskripsi Usaha"><?php echo $data->deskripsi_usaha; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Alamat</label>
				<div class="col-sm-7">
					<textarea class="form-control required" name="alamat" placeholder="Alamat"><?php echo $data->alamat; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Kota</label>
				<div class="col-sm-7">
					<select name="id_kota" class="form-control">
						<option value="">- Pilih kota -</option>
						<?php echo modules::run('options/kota', $data->id_kota); ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Lokasi (Koordinat)</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="lokasi" placeholder="Lokasi (Koordinat)" value="<?php echo $data->lokasi; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Jenis Izin Usaha</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="izin_usaha" placeholder="Jenis Izin Usaha" value="<?php echo $data->izin_usaha; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> No. Registrasi Izin</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="no_registrasi" placeholder="No. Registrasi" value="<?php echo $data->no_registrasi; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Keanggotaan Asosiasi</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="keanggotaan_asosiasi" placeholder="Keanggotaan Asosiasi" value="<?php echo $data->keanggotaan_asosiasi; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Pemilik</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="pemilik" placeholder="Nama Pemilik" value="<?php echo $data->pemilik; ?>">
				</div>
			</div>
			<!-- <div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Nama Penanggung Jawab</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="penanggung_jawab" placeholder="Nama Penanggung Jawab" value="<?php echo $data->penanggung_jawab; ?>">
				</div>
			</div> -->
		</div>
		<!-- <div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> No. Telp. Penanggung Jawab</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="no_telp" placeholder="No. Telp. Penanggung Jawab" value="<?php echo $data->no_telp; ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-5">Email Penanggung Jawab</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="email" placeholder="Email Penanggung Jawab" value="<?php echo $data->email; ?>">
				</div>
			</div>
		</div> -->
		<div class="clearfix"></div>
		<div class="separator"></div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Alat Service yg Dimiliki</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="peralatan" placeholder="Alat Service yang Dimiliki" value="<?php echo $data->peralatan; ?>">
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label col-sm-5"><span class="red">*</span> Jumlah Teknisi Bersertifikat BNSP</label>
				<div class="col-sm-7">
					<input type="text" class="form-control required" name="teknisi_bnsp" placeholder="Jumlah Teknisi Bersertifikat BNSP" value="<?php echo $data->teknisi_bnsp; ?>">
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="form-submit">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-saved"></i> Simpan Data</button>
		</div>
	</form>
</div>