<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Refrigerasi_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'refrigerasi';
		$this->foto = 'refrigrasi_foto';

		$this->like = array('nama');
		$this->filter = array();
		
		$this->fields = (object) array (
			'nama' => '',
			'profil' => '',
			'produk' => '',
			'alih_teknologi' => '',
			'nama_kontak' => '',
			'no_telp' => '',
			'email' => '',
			'lokasi' => '',
		);
	}

    public function profile($id){
        $main_table = $this->table;
        $foto_table = $this->foto;

        $this->db->select("$main_table.*");
        $this->db->where("$main_table.id = $id");

        $src = $this->db->get($main_table);
        return $src->num_rows() > 0 ? $src->row() : $this->fields;
    }

    public function images($id)
    {
        $this->db->where('deleted_at IS NULL');
        $this->db->where("id_refrigrasi = '{$id}'");
        $this->db->order_by('created_at DESC');
        $this->db->limit(3);
        return $this->db->get('refrigrasi_foto');
    }

    public function id_by($id){
        $main_table = $this->table;
        $foto_table = $this->foto;

        $this->db->select("$main_table.id AS id_refre,$main_table.nama, b.*");
        $this->db->join("$foto_table AS b", "$main_table.id = b.id_refrigrasi", 'left');
        $this->db->where("$main_table.id = $id");

        $src = $this->db->get($main_table);
        return $src->num_rows() > 0 ? $src->row() : $this->fields;
    }

    public function update_location($id, $location, $table)
    {
        $sql = "
			UPDATE {$table}
			SET lokasi = '{$location}'
			WHERE
				id = '{$id}'
				AND (
					lokasi = ''
					OR lokasi IS NULL
				)
		";
        $this->db->query($sql);
    }
	
	
}
/* End of file refrigerasi_model.php */
/* Location: ./application/modules/perusahaan/models/refrigerasi_model.php */