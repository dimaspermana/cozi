<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Besar_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'besar_vw';
        $this->foto  = 'besar_foto';
		
		$this->like = array($this->table.'.nama');
		$this->filter = array (
			$this->table.'.id' => (user_session('grup_pengguna') == 'besar') ? user_session('id_organisasi') : '',
			'id_propinsi' => (user_session('tingkatan') == '2' || user_session('id_propinsi') != NULL) ? user_session('id_propinsi'): '',
			'id_kota' => (user_session('tingkatan') == '3' || user_session('id_kota') != NULL) ? user_session('id_kota') : '',
		);
		
		$this->fields = (object) array (
			'nama' => '',
			'alamat' => '',
			'id_kota' => '',
			'npwp' => '',
			'tahun_berdiri' => '',
			'kategori' => '',
			'metode' => '',
			'id_supplier' => '',
			'penanggung_jawab' => '',
			'no_telp' => '',
			'email' => '',
			'lokasi' => '',
		);
		
		$this->list_id_propinsi = user_session('grup_pengguna') == 'balai' ? $this->session->userdata('list_id_propinsi') : '';
		
		if ($this->list_id_propinsi != '') {
			unset($this->filter['id_propinsi']);
			unset($this->filter['id_kota']);
		}
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*");
		// $this->db->select("$main_table.*, b.nama AS kota, c.nama AS propinsi");
		// $this->db->join("kota AS b", "$main_table.id_kota = b.id", 'left');
		// $this->db->join("propinsi AS c", "b.id_propinsi = c.id", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}

    public function profil($id){
        $main_table = $this->table;
        $foto_table = $this->foto;

        $this->db->select("$main_table.*, b.nama AS kota");
        $this->db->join("kota AS b", "$main_table.id_kota = b.id", 'left');
        $this->db->join("$foto_table AS f","$main_table.id = f.id_besar",'left');
        $this->db->where("$main_table.id = $id");

        $src = $this->db->get($main_table);
        return $src->num_rows() > 0 ? $src->row() : $this->fields;
    }

    public function images($id)
    {
        $this->db->where('deleted_at IS NULL');
        $this->db->where("id_besar = '{$id}'");
        $this->db->order_by('created_at DESC');
        $this->db->limit(3);
        return $this->db->get('besar_foto');
    }

    public function id_by($id){
        $main_table = $this->table;
        $foto_table = $this->foto;

        $this->db->select("$main_table.id AS id_bes,$main_table.nama, b.*");
        $this->db->join("$foto_table AS b", "$main_table.id = b.id_besar", 'left');
        $this->db->where("$main_table.id = $id");

        $src = $this->db->get($main_table);
        return $src->num_rows() > 0 ? $src->row() : $this->fields;
    }

    public function update_location($id, $location, $table)
    {
        $sql = "
			UPDATE {$table}
			SET lokasi = '{$location}'
			WHERE
				id = '{$id}'
				AND (
					lokasi = ''
					OR lokasi IS NULL
				)
		";
        $this->db->query($sql);
    }

}
/* End of file besar_model.php */
/* Location: ./application/modules/perusahaan/models/besar_model.php */