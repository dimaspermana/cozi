<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bengkel_model extends MY_Model {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->table = 'bengkel_vw';
		
		$this->like = array($this->table.'.nama');
		$this->filter = array (
			'bengkel_vw.id' => (user_session('grup_pengguna') == 'bengkel') ? user_session('id_organisasi') : '',
			'id_propinsi' => (user_session('tingkatan') == '2' || user_session('id_propinsi') != NULL) ? user_session('id_propinsi'): '',
			'id_kota' => (user_session('tingkatan') == '3' || user_session('id_kota') != NULL) ? user_session('id_kota') : '',
		);
		
		$this->fields = (object) array (
			'nama' => '',
			'jenis_usaha' => '',
			'deskripsi_usaha' => '',
			'alamat' => '',
			'id_kota' => '',
			'lokasi' => '',
			'izin_usaha' => '',
			'no_registrasi' => '',
			'keanggotaan_asosiasi' => '',
			'pemilik' => '',
			'penanggung_jawab' => '',
			'no_telp' => '',
			'email' => '',
			'peralatan' => '',
			'teknisi_bnsp' => '',
		);
		
		$this->list_id_propinsi = user_session('grup_pengguna') == 'balai' ? $this->session->userdata('list_id_propinsi') : '';
		
		if ($this->list_id_propinsi != '') {
			unset($this->filter['id_propinsi']);
			unset($this->filter['id_kota']);
		}
	}
	
	
	public function get()
	{
		$main_table = $this->table;
		$this->filter();
		
		$this->db->select("$main_table.*");
		//$this->db->select("$main_table.*, b.nama AS kota");
		//$this->db->join("kota AS b", "$main_table.id_kota = b.id", 'left');
		$this->db->order_by($this->order);
		$this->db->limit($this->limit, $this->offset);
		
		return $this->db->get($main_table);
	}
	
	
	public function profile($id)
	{
		$main_table = $this->table;
		$this->db->select("$main_table.*, b.nama AS kota");
		$this->db->join("kota AS b", "$main_table.id_kota = b.id", 'left');
		$this->db->where(array($main_table.'.id' => $id));
		return $this->db->get($main_table)->row();
	}
	
	
	public function images($id)
	{
		$this->db->where('deleted_at IS NULL');
		$this->db->where("id_bengkel = '{$id}'");
		$this->db->order_by('created_at DESC');
		$this->db->limit(3);
		return $this->db->get('bengkel_foto');
	}
	
	
	public function update_location($id, $location, $table)
	{
		$sql = "
			UPDATE {$table}
			SET lokasi = '{$location}'
			WHERE
				id = '{$id}'
				AND (
					lokasi = ''
					OR lokasi IS NULL
				)
		";
		$this->db->query($sql);
	}

	public function update_location_all($id, $location, $table)
	{
		$sql = "
			UPDATE {$table}
			SET lokasi = '{$location}'
			WHERE
				id = '{$id}'
		";
		$this->db->query($sql);
	}
	
	
	public function hpmp($id, $tahun)
	{
		$sql = "
			SELECT
				a.bulan
				, ac_05_pk
				, ac_075_pk
				, ac_1_pk
				, ac_2_pk
				, ac_mobil
				, ac_kulkas
				, ac_lainnya
				, r22
				, r134a
				, r32
				, r410a
				, r404a
				, r407a
			FROM bulan AS a
			LEFT JOIN (
				SELECT *
				FROM bengkel_laporan
				WHERE
					id_bengkel = '{$id}' AND tahun = '{$tahun}'
			) AS b
				ON a.bulan = b.bulan
			ORDER BY a.bulan
		";
		return $this->db->query($sql);
	}
	
	
}
/* End of file bengkel_model.php */
/* Location: ./application/modules/perusahaan/models/bengkel_model.php */