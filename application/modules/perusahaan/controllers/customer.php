<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends MX_Controller {
	
	
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('supplier_model');
		$this->load->model('costumer_model');
	}
	
	
	public function index($q_encoded = 'x')
	{
		$this->costumer_model->set_filter($q_encoded);
		
		$this->grid->init(array (
			'base_url' => $this->page->base_url("/index/$q_encoded"),
			'act_url' => $this->page->base_url(),
			'uri_segment' => 5,
			'items'	=> array (
                'created_at' => array('text' => 'Waktu Input'),
				'nama' => array('text' => 'Nama Costumer'),
				'alamat' => array('text' => 'Alamat'),
				'no_telp' => array('text' => 'No Telpon', 'align' => 'center'),
				'supplier' => array('text' => 'Nama Supplier'),
			),
			'num_rows' => $this->costumer_model->num_rows(),
			'item' => 'nama',
			'order' => 'asc',
			'warning' => 'nama',
			'checkbox' => FALSE,
		));
		
		$this->costumer_model->set_grid_params($this->grid->params());
		$this->grid->source($this->costumer_model->get());
		
		$this->page->view('costumer_supplier_index', array (
			'add_url' => $this->page->base_url('/tambah'),
			'supplier_show' => site_url('/perusahaan/supplier'),
			'insert_url' => $this->page->base_url('/insert'),
			'show_url' => $this->page->base_url('/show'),
			'filter' => (object) $this->costumer_model->filter,
			'keyword' => $this->costumer_model->keyword,
			'grid' => $this->grid->draw(),
			'pagelink' => $this->grid->page_link(),
		));
	}
	
	
	public function show()
	{
		$q_encoded = base64_encode(json_encode($this->input->post()));
		redirect($this->page->base_url("/index/$q_encoded"));
	}
	
	
	public function form($action = 'insert', $id = '')
	{
		if ($this->agent->referrer() == '') redirect($this->page->base_url());
		
		$this->page->view('customer_form', array (
			'action_url' => $this->page->base_url("/$action/$id"),
			'redirect' => $this->agent->referrer(),
			'data' => $this->costumer_model->by_id($id),
		));
	}
	
	
	public function tambah()
	{
		$this->form();
	}
	
	
	public function ubah($id)
	{
		$this->form('update', $id);
	}
	
	
	public function form_data()
	{
		$names = array('id_supplier','upper_nama', 'upper_alamat','no_telp');
		return form_data($names);
	}
	
	
	public function insert()
	{
		$data = $this->form_data();
		db_insert('supplier_customer', $data);
		
		$id_sup = $this->input->post('id_supplier');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_telp = $this->input->post('no_telp');
		
		$src = $this->db->query("SELECT sc.*, s.id_kota
		from supplier_customer sc
		left join supplier s on s.id = sc.id_supplier
		where sc.id_supplier = '$id_sup' and sc.nama = '$nama' and sc.alamat = '$alamat' and sc.no_telp = '$no_telp' ")->row();
		
		$dauk = array (
		   'nama' => $src->nama,  
		   'alamat'  => $src->alamat, 
		   'no_telp'  => $src->no_telp, 
		   'id_kota'  => $src->id_kota, 
		   'id_supplier'  => $src->id_supplier, 
		   'id_customer'  => $src->id, 
		   'penanggung_jawab'=> $src->nama
		  ); 
		  
		db_insert('ukm', $dauk);
		
		redirect($this->input->post('redirect'));
	}
	
	
	public function update($id)
	{
		$data = $this->form_data();
		db_update('supplier_customer', $data, array('id' => $id));
		
		$src = $this->db->query("SELECT sc.*, s.id_kota
		from supplier_customer sc
		left join supplier s on s.id = sc.id_supplier
		where sc.id = '$id'  ")->row();
		
		$dauk = array (
		   'nama' => $src->nama,  
		   'alamat'  => $src->alamat, 
		   'no_telp'  => $src->no_telp, 
		   'id_kota'  => $src->id_kota, 
		   'id_supplier'  => $src->id_supplier, 
		   'penanggung_jawab'=> $src->nama
		  ); 
		  
		db_update('ukm', $dauk, array('id_customer' => $id));
		
		redirect($this->input->post('redirect'));
	}
	
	public function hapus($id)
	{
		if ($this->agent->referrer() == '') show_404();
		db_delete('supplier_customer', array('id' => $id));
		
		db_delete('ukm', array('id_customer' => $id));
		
		redirect($this->agent->referrer());
	}
	
}

/* End of file costumer.php */
/* Location: ./application/modules/perusahaan/controllers/costumer.php */