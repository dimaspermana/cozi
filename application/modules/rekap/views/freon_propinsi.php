<div id="header" class="container-fluid">
	<h1 class="col-sm-6">Rekap Penggunaan Freon Propinsi</h1>
</div>
<div id="main-container" class="container-fluid">
	<div class="clearfix"></div>
	<br>
	<form method="post" action="<?php echo $show_url; ?>">
		<div class="actions">
			Tahun
			<select class="col-sm-2 col-md-2" name="tahun">
				<?php echo modules::run('options/tahun', $tahun); ?>
			</select>
			<input type="checkbox" name="kriteria" id="kriteria" value="1" 
			<?php if ($krit == '1') echo "checked"; ?> onclick="get_bln(this);" >
			<input type="hidden" id="blns" value=<?php echo $krit; ?>>
			Bulan
			<select class="col-sm-2 col-md-2" name="bulan" id="bulan" disabled>
				<?php echo modules::run('options/bulan', $bulan); ?>
			</select>
			<?php if (user_session('tingkatan') == '1'): ?>
			Propinsi
			<select class="col-sm-2 col-md-2" name="id_propinsi">
				<?php echo modules::run('options/propinsi', $id_propinsi); ?>
			</select>
			<?php endif; ?>	
			<button class="btn btn-primary"><i class="glyphicon glyphicon-refresh"></i></button>
			<div class="clearfix"></div>
		</div>
	</form>
	<div id="num-data"><i class="glyphicon glyphicon-stats"></i>&nbsp; <?php echo $total_data; ?> data</div>
	<!-- <?php echo $grid; ?> -->
	<!-- <?php echo $pagelink; ?> -->
	<div class="col-sm-12">
	<div class="table-responsive">
		<table class="table table-profile table-striped table-condensed">
			<thead>
				<tr>
					<th>No.</th>
					<th>Kota/Kab.</th>
					<th>R-22</th>
					<th>R-123</th>
					<th>R-134A</th>
					<th>R-32</th>
					<th>R-410A</th>
					<th>R-404A</th>
					<th>R-407A</th>
					<th>R-290</th>
					<th>R-600A</th>
					<th>R-717</th>
					<th>R-744</th>
					<th>TOTAL</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no=$offset;
					$totalr22=0;
					$totalr123=0;
					$totalr134a=0;
					$totalr32=0;
					$totalr410a=0;
					$totalr404a=0;
					$totalr407a=0;
					$totalr290=0;
					$totalr600a=0;
					$totalamonia=0;
					$totalco2=0;
					$totaltotal=0;
					foreach ($propinsi->result() as $p): 
					$no++;
					$total=$p->r22+$p->r123+$p->r134a+$p->r32+$p->r410a+$p->r404a+$p->r407a+$p->r290+$p->r600a+$p->amonia+$p->co2;
				?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><a href="<?php echo $this->page->base_url('/profil_kota/'.$p->id.'/'.$tahun.'/'.$bulan.'/'.$krit); ?>"><?php echo $p->nama; ?></td>
					<td align="center"><?php echo $p->r22; ?></td>
					<td align="center"><?php echo $p->r123; ?></td>
					<td align="center"><?php echo $p->r134a; ?></td>
					<td align="center"><?php echo $p->r32; ?></td>
					<td align="center"><?php echo $p->r410a; ?></td>
					<td align="center"><?php echo $p->r404a; ?></td>
					<td align="center"><?php echo $p->r407a; ?></td>
					<td align="center"><?php echo $p->r290; ?></td>
					<td align="center"><?php echo $p->r600a; ?></td>
					<td align="center"><?php echo $p->amonia; ?></td>
					<td align="center"><?php echo $p->co2; ?></td>
					<td align="center"><?php echo round($total,2); ?></td>
				</tr>
				<?php 
					$totalr22+=$p->r22;
					$totalr123+=$p->r123;
					$totalr134a+=$p->r134a;
					$totalr32+=$p->r32;
					$totalr410a+=$p->r410a;
					$totalr404a+=$p->r404a;
					$totalr407a+=$p->r407a;
					$totalr290+=$p->r290;
					$totalr600a+=$p->r600a;
					$totalamonia+=$p->amonia;
					$totalco2+=$p->co2;
					$totaltotal+=$total;
					endforeach; 
				?>
				<tr>
					<td colspan="2">TOTAL</td>
					<td align="center"><?php echo round($totalr22,2); ?></td>
					<td align="center"><?php echo round($totalr123,2); ?></td>
					<td align="center"><?php echo round($totalr134a,2); ?></td>
					<td align="center"><?php echo round($totalr32,2); ?></td>
					<td align="center"><?php echo round($totalr410a,2); ?></td>
					<td align="center"><?php echo round($totalr404a,2); ?></td>
					<td align="center"><?php echo round($totalr407a,2); ?></td>
					<td align="center"><?php echo round($totalr290,2); ?></td>
					<td align="center"><?php echo round($totalr600a,2); ?></td>
					<td align="center"><?php echo round($totalamonia,2); ?></td>
					<td align="center"><?php echo round($totalco2,2); ?></td>
					<td align="center"><?php echo round($totaltotal,2); ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	</div>
	<?php echo $page_link; ?> 
</div>
<script>
$().ready(function() {
if($('#blns').val()==1){
	$("#bulan").attr("disabled",false);
}else{
	$("#bulan").attr("disabled",true);
}
});

function get_bln(obj) {
    if(obj.checked) {
		$("#bulan").attr("disabled",false);
	}else{
		$("#bulan").attr("disabled",true);
	}
};
</script>
<style type="text/css" media="screen">
	.divkiri{
		float:left;
		width: 80%;
	}
	.divkanan{
		float: right;
		width: 15%;
	}
	.outer {
	    display: table;
	    position: absolute;
	    height: 100%;
	    width: 100%;
	}

	.middle {
	    display: table-cell;
	    vertical-align: middle;
	}

	.inner {
	    margin-left: auto;
	    margin-right: auto; 
	    /*width: /*whatever width you want*/;*/
	}
</style>
	