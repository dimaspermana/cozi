<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HCFC extends MX_Controller {
	
	private $_num_page = 0;
	private $_page = 1;
	private $_limit = 20;
	private $_base_url = "";
	public function __construct()
	{
		parent::__construct();
		$this->page->use_directory();
		$this->load->model('hcfc_model');
	}
	
	public function index($q_encoded='', $k_='')
	{
		if($q_encoded==null)
			$q_encoded=date('Y');
		if($k_==null)
			$k_='hcfc';

		if(user_session('tingkatan')==1){
			$this->profil_nasional($q_encoded, $k_);
		}elseif (user_session('tingkatan')==2) {
			$this->profil(user_session('id_propinsi'),$q_encoded, $b_, $k_);
		}elseif (user_session('tingkatan')==3) {
			$this->profil_kota(user_session('id_kota'),$q_encoded, $b_, $k_);
		}


	}

	public function profil_nasional($tahun,$krit)
	{	
		$num_rows = $this->hcfc_model->get($tahun,$krit)->num_rows();
		$this->page->view('hcfc_index', array (
				'propinsi' 	=> $this->hcfc_model->get($tahun,$krit),
				'tahun' 	=> $tahun,
				'krit' 		=> $krit,
				'total_data'=> $num_rows,
				'show_url' 	=> $this->page->base_url('/show'),
		));
	}

	public function show()
	{
		$q_encoded 	= $this->input->post('tahun');
		$k_ 		= $this->input->post('kriteria');
		redirect($this->page->base_url("/index/$q_encoded/$k_"));
	}

	public function profil($id_propinsi, $tahun, $krit)
	{
		$num_rows = $this->hcfc_model->get_kota($id_propinsi, $tahun, $krit)->num_rows();
		$this->_num_page  = ceil($num_rows/$this->_limit);
		$this->_page = $this->uri->segment(7);
		$this->_page = $this->_page == ""? 1:$this->_page;
		$off = ($this->_page-1)*$this->_limit;
	
		$this->page->view('hcfc_propinsi', array (
				'propinsi' 		=> $this->hcfc_model->get_kota($id_propinsi, $tahun, $krit, $off, $this->_limit),
				'tahun' 		=> $tahun,
				'id_propinsi' 	=> $id_propinsi,
				'krit' 			=> $krit,
				'total_data'	=> $num_rows,
				'show_url' 		=> $this->page->base_url('/show_profil'),
				'page_link'		=> $this->page_link('profil', $id_propinsi, $tahun, $krit),
				'offset'		=> $off,
		));
	}

	public function profil_kota($id_kota, $tahun, $krit)
	{
		$num_rows = $this->hcfc_model->get_kota_detil($id_kota, $tahun, $krit)->num_rows();
		$this->_num_page  = ceil($num_rows/$this->_limit);
		$this->_page = $this->uri->segment(7);
		$this->_page = $this->_page == ""? 1:$this->_page;
		$off = ($this->_page-1)*$this->_limit;
	
		$this->page->view('hcfc_kota', array (
				'propinsi' 		=> $this->hcfc_model->get_kota_detil($id_kota, $tahun, $krit, $off, $this->_limit),
				'tahun' 		=> $tahun,
				'id_kota' 		=> $id_kota,
				'krit' 			=> $krit,
				'total_data'	=> $num_rows,
				'show_url' 		=> $this->page->base_url('/show_profil_kota'),
				'page_link'		=> $this->page_link('profil_kota', $id_kota, $tahun, $krit),
				'offset'		=> $off,
		));
	}	

	public function show_profil()
	{
		$id_propinsi = $this->input->post('id_propinsi')==''? user_session('id_propinsi'):$this->input->post('id_propinsi');
		$tahun = $this->input->post('tahun');
		$k_    = $this->input->post('kriteria');
		redirect($this->page->base_url("/profil/$id_propinsi/$tahun/$k_"));
	}

	public function show_profil_kota()
	{
		$id_kota = $this->input->post('id_kota')==''? user_session('id_kota'):$this->input->post('id_kota');
		$tahun = $this->input->post('tahun');
		$k_    = $this->input->post('kriteria');
		redirect($this->page->base_url("/profil_kota/$id_kota/$tahun/$k_"));
	}

	function page_link($fungsi, $id_lokasi, $tahun, $krit)
	{
		$show = 5;
		$half = floor($show / 2);
		$countdown = $show - 1;
		
		$this->_base_url = base_url('/rekap/hcfc/'.$fungsi.'/'.$id_lokasi.'/'. $tahun.'/'.$krit);
		// tidak ada data
		if ($this->_num_page == 0) return '';
		
		// buka ul
		$retval = '<nav><ul class="pagination">';
		
		// link 'sebelumnya'
		if ($this->_page > 1) {
			$prev = $this->_page - 1;
			// $retval .= '<li><a href="'.$this->_base_url.'/'.$prev.'" aria-label="Previous"><span aria-hidden="true">&lsaquo;</span></a></li>';
		}
		
		// print halaman 1
		$current = ($this->_page == 1 ? ' class="active"' : '');
		$retval .= '<li'.$current.'><a href="'.$this->_base_url.'/1/'.$this->_limit.'">1</a></li>';
		
		// halaman kedua
		if ($this->_page >= ($this->_num_page - $half)) {
			$counter = $this->_num_page - $countdown;
		}
		else {
			$counter = $this->_page - $half;
		}
		
		if ($counter <= 1) {
			$show--;
			$counter = 2;
		}
		
		if ($counter > 2) {
			$retval .= '<li class="disabled"><a href="#">...</a></li>';
		}
		
		$stop = ($counter >= $this->_num_page);
		
		while ( ! $stop) {
			$current = ($this->_page == $counter ? ' class="active"' : '');
			$retval .= '<li'.$current.'><a href="'.$this->_base_url.'/'.$counter.'/'.$this->_limit.'">'.$counter.'</a></li>';
			$show--;
			$counter++;
			$stop = ($show == 0 OR $counter == $this->_num_page);
		}
		
		// halaman terakhir
		$second_last = $this->_num_page - 1;
		if ($counter <= $second_last) {
			$retval .= '<li class="disabled"><a href="#">...</a></li>';
		}
		
		if ($counter <= $this->_num_page) {
			$current = ($this->_page == $this->_num_page ? ' class="active"' : '');
			$retval .= '<li'.$current.'><a href="'.$this->_base_url.'/'.$this->_num_page.'/'.$this->_limit.'">'.$this->_num_page.'</a></li>';
		}
		
		// link 'selanjutnya'
		if ($this->_page < $this->_num_page) {
			$next = $this->_page + 1;
			// $retval .= '<li><a href="'.$this->_base_url.'/'.$next.'" aria-label="Next"><span aria-hidden="true">&rsaquo;</span></a></li>';
		}
		
		// tutup ul dan return
		$retval .= '</ul></nav>';
		return $retval;
	}
	
}

/* End of file hcfc.php */
/* Location: ./application/modules/rekap/controllers/hcfc.php */