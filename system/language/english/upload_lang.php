<?php

$lang['upload_userfile_not_set'] = "Tidak ada variabel HTTP Post dengan nama userfile";
$lang['upload_file_exceeds_limit'] = "Ukuran file terlalu besar";
$lang['upload_file_exceeds_form_limit'] = "Ukuran file terlalu besar";
$lang['upload_file_partial'] = "File corrupt";
$lang['upload_no_temp_directory'] = "Direktori temp hilang";
$lang['upload_unable_to_write_file'] = "File tidak dapat disimpan di server";
$lang['upload_stopped_by_extension'] = "Ekstensi file tidak valid";
$lang['upload_no_file_selected'] = "Tidak ada file yang di-upload";
$lang['upload_invalid_filetype'] = "Tipe file tidak valid";
$lang['upload_invalid_filesize'] = "Ukuran file terlalu besar";
$lang['upload_invalid_dimensions'] = "Dimensi gambar terlalu besar";
$lang['upload_destination_error'] = "Kesalahan proses di server";
$lang['upload_no_filepath'] = "Upload path tidak valid";
$lang['upload_no_file_types'] = "Tipe file tidak valid";
$lang['upload_bad_filename'] = "File dengan nama yang sama sudah ada di server";
$lang['upload_not_writable'] = "Direktori upload tidak dapat ditulis";


/* End of file upload_lang.php */
/* Location: ./system/language/english/upload_lang.php */