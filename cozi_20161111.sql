-- create supplier_laporan_vw
CREATE ALGORITHM = UNDEFINED VIEW `supplier_laporan_vw` AS 
SELECT ul. * , u.id_kota AS idkota, k.id_propinsi, sc.nama AS customer
FROM supplier_laporan ul
JOIN supplier u ON u.id = ul.id_supplier
JOIN supplier_customer sc ON sc.id = ul.id_customer
JOIN kota k ON k.id = u.id_kota;

-- add column jenis_kegiatan
ALTER TABLE `institusi_laporan` ADD `jenis_kegiatan` ENUM( '0', '1' ) NOT NULL AFTER `sertifikasi`;




