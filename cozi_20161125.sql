CREATE TABLE IF NOT EXISTS `monitoring` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `tgl_monitoring` date DEFAULT NULL,
  `id_organisasi` int(11) NOT NULL,
  `perusahaan` varchar(50) NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `monitoring_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_monitoring` int(11) NOT NULL,
  `nama_file` varchar(200) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE ALGORITHM=UNDEFINED VIEW `perusahaan_vw` AS select `bengkel`.`id` AS `id`,`bengkel`.`nama` AS `nama`,'bengkel' AS `perusahaan` from `bengkel` where isnull(`bengkel`.`deleted_at`) union all select `ukm`.`id` AS `id`,`ukm`.`nama` AS `nama`,'ukm' AS `perusahaan` from `ukm` where isnull(`ukm`.`deleted_at`) union all select `institusi`.`id` AS `id`,`institusi`.`nama` AS `nama`,'institusi' AS `perusahaan` from `institusi` where isnull(`institusi`.`deleted_at`) union all select `supplier`.`id` AS `id`,`supplier`.`nama` AS `nama`,'supplier' AS `perusahaan` from `supplier` where isnull(`supplier`.`deleted_at`) union all select `besar`.`id` AS `id`,`besar`.`nama` AS `nama`,'besar' AS `perusahaan` from `besar` where isnull(`besar`.`deleted_at`);

-- create institusi_laporan_vw
CREATE ALGORITHM = UNDEFINED VIEW `institusi_laporan_vw` AS SELECT ul . * , u.id_kota AS idkota, k.id_propinsi
FROM institusi_laporan ul
JOIN institusi u ON u.id = ul.id_institusi
JOIN kota k ON k.id = u.id_kota;

CREATE TABLE IF NOT EXISTS `supplier_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_supplier` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `keterangan` text,
  `lokasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `ukm_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_ukm` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `keterangan` text,
  `lokasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `besar_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_besar` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `keterangan` text,
  `lokasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `ac_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_ac` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `keterangan` text,
  `lokasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `refrigrasi_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_refrigrasi` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `keterangan` text,
  `lokasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `institusi_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_institusi` int(11) NOT NULL,
  `nama_file` varchar(50) NOT NULL,
  `keterangan` text,
  `lokasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;