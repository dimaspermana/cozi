ALTER TABLE `tips` ADD `jenis_media` ENUM( "0", "1" ) NOT NULL AFTER `tgl_publish` ,
ADD `url` TEXT NOT NULL AFTER `jenis_media` ;

CREATE TABLE IF NOT EXISTS `meeting_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_meeting` int(11) NOT NULL,
  `jenis_media` ENUM( "0", "1" ) NOT NULL,
  `nama_file` varchar(50) NULL,
  `url` text(100) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `sosialisasi_foto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `id_sosialisasi` int(11) NOT NULL,
  `jenis_media` ENUM( "0", "1" ) NOT NULL,
  `nama_file` varchar(50) NULL,
  `url` text(100) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

