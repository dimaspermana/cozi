-- create view bengkel_laphar_vw

CREATE ALGORITHM=UNDEFINED VIEW `bengkel_laphar_vw` AS
select bl.*, b.id_kota , k.id_propinsi
from 
bengkel_laphar bl
left join bengkel b on b.id = bl.id_bengkel
left join kota k on k.id = b.id_kota
left join propinsi p on p.id = k.id_propinsi