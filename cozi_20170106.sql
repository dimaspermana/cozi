ALTER TABLE `pengaduan` ADD `id_kategori` INT NOT NULL AFTER `tgl_posting` ,
ADD `status` ENUM( '0', '1' ) NOT NULL AFTER `id_kategori` ;

CREATE TABLE IF NOT EXISTS `pengaduan_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_at` datetime,
  `deleted_by` int(11) DEFAULT NULL,
  `nama` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `pengaduan` 
ADD `lokasi` VARCHAR(200) NOT NULL AFTER `id_kategori` ;