-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 06, 2016 at 09:22 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cozi`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `bengkel_laporan_vw`
--
CREATE TABLE IF NOT EXISTS `bengkel_laporan_vw` (
`id` int(11)
,`created_at` timestamp
,`created_by` int(11)
,`updated_at` datetime
,`updated_by` int(11)
,`deleted_at` datetime
,`deleted_by` int(11)
,`id_bengkel` int(11)
,`tahun` char(4)
,`bulan` char(2)
,`ac_05_pk` int(11)
,`ac_075_pk` int(11)
,`ac_1_pk` int(11)
,`ac_2_pk` int(11)
,`ac_mobil` int(11)
,`ac_kulkas` int(11)
,`ac_lainnya` int(11)
,`r22` double
,`r134a` double
,`r32` double
,`r410a` double
,`r404a` double
,`r407a` double
,`r123` double
,`hidrokarbon` double
,`amonia` double
,`co2` double
,`ac_05_2` int(11)
,`ac_3_10` int(11)
,`ac_10` int(11)
,`ac_cold` int(11)
,`r290` int(11)
,`r600a` int(11)
,`idkota` int(11)
,`id_propinsi` int(11)
);
-- --------------------------------------------------------

--
-- Table structure for table `bengkel_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`bengkel_vw` AS select `b`.`id` AS `id`,`b`.`created_at` AS `created_at`,`b`.`created_by` AS `created_by`,`b`.`updated_at` AS `updated_at`,`b`.`updated_by` AS `updated_by`,`b`.`deleted_at` AS `deleted_at`,`b`.`deleted_by` AS `deleted_by`,`b`.`nama` AS `nama`,`b`.`jenis_usaha` AS `jenis_usaha`,`b`.`deskripsi_usaha` AS `deskripsi_usaha`,`b`.`alamat` AS `alamat`,`b`.`kota` AS `kota`,`b`.`id_kota` AS `id_kota`,`b`.`lokasi` AS `lokasi`,`b`.`izin_usaha` AS `izin_usaha`,`b`.`no_registrasi` AS `no_registrasi`,`b`.`keanggotaan_asosiasi` AS `keanggotaan_asosiasi`,`b`.`pemilik` AS `pemilik`,`b`.`penanggung_jawab` AS `penanggung_jawab`,`b`.`no_telp` AS `no_telp`,`b`.`email` AS `email`,`b`.`peralatan` AS `peralatan`,`b`.`teknisi_bnsp` AS `teknisi_bnsp`,`k`.`id_propinsi` AS `id_propinsi`,`k`.`nama` AS `kotane`,`p`.`nama` AS `propinsi` from ((`cozi`.`bengkel` `b` left join `cozi`.`kota` `k` on((`k`.`id` = `b`.`id_kota`))) left join `cozi`.`propinsi` `p` on((`p`.`id` = `k`.`id_propinsi`)));

-- --------------------------------------------------------

--
-- Table structure for table `besar_laporan_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`besar_laporan_vw` AS select `ul`.`id` AS `id`,`ul`.`created_at` AS `created_at`,`ul`.`created_by` AS `created_by`,`ul`.`updated_at` AS `updated_at`,`ul`.`updated_by` AS `updated_by`,`ul`.`deleted_at` AS `deleted_at`,`ul`.`deleted_by` AS `deleted_by`,`ul`.`id_besar` AS `id_besar`,`ul`.`tahun` AS `tahun`,`ul`.`bulan` AS `bulan`,`ul`.`pembelian_hcfc` AS `pembelian_hcfc`,`ul`.`pembelian_preblended` AS `pembelian_preblended`,`u`.`id_kota` AS `idkota`,`k`.`id_propinsi` AS `id_propinsi` from ((`cozi`.`besar_laporan` `ul` join `cozi`.`besar` `u` on((`u`.`id` = `ul`.`id_besar`))) join `cozi`.`kota` `k` on((`k`.`id` = `u`.`id_kota`)));

-- --------------------------------------------------------

--
-- Table structure for table `besar_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`besar_vw` AS select `b`.`id` AS `id`,`b`.`created_at` AS `created_at`,`b`.`created_by` AS `created_by`,`b`.`updated_at` AS `updated_at`,`b`.`updated_by` AS `updated_by`,`b`.`deleted_at` AS `deleted_at`,`b`.`deleted_by` AS `deleted_by`,`b`.`nama` AS `nama`,`b`.`alamat` AS `alamat`,`b`.`id_kota` AS `id_kota`,`b`.`lokasi` AS `lokasi`,`b`.`npwp` AS `npwp`,`b`.`tahun_berdiri` AS `tahun_berdiri`,`b`.`kategori` AS `kategori`,`b`.`metode` AS `metode`,`b`.`id_supplier` AS `id_supplier`,`b`.`penanggung_jawab` AS `penanggung_jawab`,`b`.`no_telp` AS `no_telp`,`b`.`email` AS `email`,`k`.`id_propinsi` AS `id_propinsi`,`k`.`nama` AS `kota`,`p`.`nama` AS `propinsi` from ((`cozi`.`besar` `b` left join `cozi`.`kota` `k` on((`k`.`id` = `b`.`id_kota`))) left join `cozi`.`propinsi` `p` on((`p`.`id` = `k`.`id_propinsi`)));

-- --------------------------------------------------------

--
-- Table structure for table `institusi_laporan_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`institusi_laporan_vw` AS select `ul`.`id` AS `id`,`ul`.`created_at` AS `created_at`,`ul`.`created_by` AS `created_by`,`ul`.`updated_at` AS `updated_at`,`ul`.`updated_by` AS `updated_by`,`ul`.`deleted_at` AS `deleted_at`,`ul`.`deleted_by` AS `deleted_by`,`ul`.`id_institusi` AS `id_institusi`,`ul`.`tgl_pelatihan` AS `tgl_pelatihan`,`ul`.`peserta` AS `peserta`,`ul`.`peserta_lulus` AS `peserta_lulus`,`ul`.`sertifikasi` AS `sertifikasi`,`ul`.`jenis_kegiatan` AS `jenis_kegiatan`,`u`.`id_kota` AS `idkota`,`k`.`id_propinsi` AS `id_propinsi` from ((`cozi`.`institusi_laporan` `ul` join `cozi`.`institusi` `u` on((`u`.`id` = `ul`.`id_institusi`))) join `cozi`.`kota` `k` on((`k`.`id` = `u`.`id_kota`)));

-- --------------------------------------------------------

--
-- Table structure for table `institusi_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`institusi_vw` AS select `b`.`id` AS `id`,`b`.`created_at` AS `created_at`,`b`.`created_by` AS `created_by`,`b`.`updated_at` AS `updated_at`,`b`.`updated_by` AS `updated_by`,`b`.`deleted_at` AS `deleted_at`,`b`.`deleted_by` AS `deleted_by`,`b`.`nama` AS `nama`,`b`.`alamat` AS `alamat`,`b`.`id_kota` AS `id_kota`,`b`.`lokasi` AS `lokasi`,`b`.`tuk` AS `tuk`,`b`.`tgl_berlaku` AS `tgl_berlaku`,`b`.`profil` AS `profil`,`b`.`peralatan_rac` AS `peralatan_rac`,`b`.`peralatan_bantuan` AS `peralatan_bantuan`,`b`.`jenis_pelatihan` AS `jenis_pelatihan`,`b`.`nama_kontak` AS `nama_kontak`,`b`.`no_telp` AS `no_telp`,`b`.`email` AS `email`,`k`.`id_propinsi` AS `id_propinsi`,`k`.`nama` AS `kota`,`p`.`nama` AS `propinsi` from ((`cozi`.`institusi` `b` left join `cozi`.`kota` `k` on((`k`.`id` = `b`.`id_kota`))) left join `cozi`.`propinsi` `p` on((`p`.`id` = `k`.`id_propinsi`)));

-- --------------------------------------------------------

--
-- Table structure for table `pengguna_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`pengguna_vw` AS select `p`.`id` AS `id`,`p`.`created_at` AS `created_at`,`p`.`created_by` AS `created_by`,`p`.`updated_at` AS `updated_at`,`p`.`updated_by` AS `updated_by`,`p`.`deleted_at` AS `deleted_at`,`p`.`deleted_by` AS `deleted_by`,`p`.`nama` AS `nama`,`p`.`no_hp` AS `no_hp`,`p`.`email` AS `email`,`p`.`user_id` AS `user_id`,`p`.`password` AS `password`,`p`.`initial_pwd` AS `initial_pwd`,`p`.`grup_pengguna` AS `grup_pengguna`,`p`.`tingkatan` AS `tingkatan`,`p`.`id_organisasi` AS `id_organisasi`,`pn`.`id_kota` AS `id_kota`,`pn`.`id_propinsi` AS `id_propinsi` from (`cozi`.`pengguna` `p` left join `cozi`.`pemerintah` `pn` on(((`pn`.`id` = `p`.`id_organisasi`) and (`p`.`grup_pengguna` = 'pemerintah'))));

-- --------------------------------------------------------

--
-- Table structure for table `perusahaan_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`perusahaan_vw` AS select `cozi`.`bengkel`.`id` AS `id`,`cozi`.`bengkel`.`nama` AS `nama`,'bengkel' AS `perusahaan` from `cozi`.`bengkel` where isnull(`cozi`.`bengkel`.`deleted_at`) union all select `cozi`.`ukm`.`id` AS `id`,`cozi`.`ukm`.`nama` AS `nama`,'ukm' AS `perusahaan` from `cozi`.`ukm` where isnull(`cozi`.`ukm`.`deleted_at`) union all select `cozi`.`institusi`.`id` AS `id`,`cozi`.`institusi`.`nama` AS `nama`,'institusi' AS `perusahaan` from `cozi`.`institusi` where isnull(`cozi`.`institusi`.`deleted_at`) union all select `cozi`.`supplier`.`id` AS `id`,`cozi`.`supplier`.`nama` AS `nama`,'supplier' AS `perusahaan` from `cozi`.`supplier` where isnull(`cozi`.`supplier`.`deleted_at`) union all select `cozi`.`besar`.`id` AS `id`,`cozi`.`besar`.`nama` AS `nama`,'besar' AS `perusahaan` from `cozi`.`besar` where isnull(`cozi`.`besar`.`deleted_at`);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_laporan_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`supplier_laporan_vw` AS select `ul`.`id` AS `id`,`ul`.`created_at` AS `created_at`,`ul`.`created_by` AS `created_by`,`ul`.`updated_at` AS `updated_at`,`ul`.`updated_by` AS `updated_by`,`ul`.`deleted_at` AS `deleted_at`,`ul`.`deleted_by` AS `deleted_by`,`ul`.`id_supplier` AS `id_supplier`,`ul`.`tahun` AS `tahun`,`ul`.`bulan` AS `bulan`,`ul`.`id_customer` AS `id_customer`,`ul`.`produk` AS `produk`,`ul`.`penjualan` AS `penjualan`,`u`.`id_kota` AS `idkota`,`k`.`id_propinsi` AS `id_propinsi`,`sc`.`nama` AS `customer` from (((`cozi`.`supplier_laporan` `ul` join `cozi`.`supplier` `u` on((`u`.`id` = `ul`.`id_supplier`))) join `cozi`.`supplier_customer` `sc` on((`sc`.`id` = `ul`.`id_customer`))) join `cozi`.`kota` `k` on((`k`.`id` = `u`.`id_kota`)));

-- --------------------------------------------------------

--
-- Table structure for table `supplier_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`supplier_vw` AS select `b`.`id` AS `id`,`b`.`created_at` AS `created_at`,`b`.`created_by` AS `created_by`,`b`.`updated_at` AS `updated_at`,`b`.`updated_by` AS `updated_by`,`b`.`deleted_at` AS `deleted_at`,`b`.`deleted_by` AS `deleted_by`,`b`.`nama` AS `nama`,`b`.`alamat` AS `alamat`,`b`.`id_kota` AS `id_kota`,`b`.`lokasi` AS `lokasi`,`b`.`npwp` AS `npwp`,`b`.`tahun_berdiri` AS `tahun_berdiri`,`b`.`penanggung_jawab` AS `penanggung_jawab`,`b`.`no_telp` AS `no_telp`,`b`.`email` AS `email`,`k`.`id_propinsi` AS `id_propinsi`,`k`.`nama` AS `kota`,`p`.`nama` AS `propinsi` from ((`cozi`.`supplier` `b` left join `cozi`.`kota` `k` on((`k`.`id` = `b`.`id_kota`))) left join `cozi`.`propinsi` `p` on((`p`.`id` = `k`.`id_propinsi`)));

-- --------------------------------------------------------

--
-- Table structure for table `ukm_laporan_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`ukm_laporan_vw` AS select `ul`.`id` AS `id`,`ul`.`created_at` AS `created_at`,`ul`.`created_by` AS `created_by`,`ul`.`updated_at` AS `updated_at`,`ul`.`updated_by` AS `updated_by`,`ul`.`deleted_at` AS `deleted_at`,`ul`.`deleted_by` AS `deleted_by`,`ul`.`id_ukm` AS `id_ukm`,`ul`.`tahun` AS `tahun`,`ul`.`bulan` AS `bulan`,`ul`.`pembelian_preblended` AS `pembelian_preblended`,`u`.`id_kota` AS `idkota`,`k`.`id_propinsi` AS `id_propinsi` from ((`cozi`.`ukm_laporan` `ul` join `cozi`.`ukm` `u` on((`u`.`id` = `ul`.`id_ukm`))) join `cozi`.`kota` `k` on((`k`.`id` = `u`.`id_kota`)));

-- --------------------------------------------------------

--
-- Table structure for table `ukm_vw`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cozi`.`ukm_vw` AS select `b`.`id` AS `id`,`b`.`created_at` AS `created_at`,`b`.`created_by` AS `created_by`,`b`.`updated_at` AS `updated_at`,`b`.`updated_by` AS `updated_by`,`b`.`deleted_at` AS `deleted_at`,`b`.`deleted_by` AS `deleted_by`,`b`.`nama` AS `nama`,`b`.`alamat` AS `alamat`,`b`.`id_kota` AS `id_kota`,`b`.`lokasi` AS `lokasi`,`b`.`npwp` AS `npwp`,`b`.`kategori` AS `kategori`,`b`.`metode` AS `metode`,`b`.`id_supplier` AS `id_supplier`,`b`.`id_customer` AS `id_customer`,`b`.`penanggung_jawab` AS `penanggung_jawab`,`b`.`no_telp` AS `no_telp`,`b`.`email` AS `email`,`k`.`id_propinsi` AS `id_propinsi`,`k`.`nama` AS `kota`,`p`.`nama` AS `propinsi` from ((`cozi`.`ukm` `b` left join `cozi`.`kota` `k` on((`k`.`id` = `b`.`id_kota`))) left join `cozi`.`propinsi` `p` on((`p`.`id` = `k`.`id_propinsi`)));

-- --------------------------------------------------------

--
-- Structure for view `bengkel_laporan_vw`
--
DROP TABLE IF EXISTS `bengkel_laporan_vw`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `bengkel_laporan_vw` AS select `ul`.`id` AS `id`,`ul`.`created_at` AS `created_at`,`ul`.`created_by` AS `created_by`,`ul`.`updated_at` AS `updated_at`,`ul`.`updated_by` AS `updated_by`,`ul`.`deleted_at` AS `deleted_at`,`ul`.`deleted_by` AS `deleted_by`,`ul`.`id_bengkel` AS `id_bengkel`,`ul`.`tahun` AS `tahun`,`ul`.`bulan` AS `bulan`,`ul`.`ac_05_pk` AS `ac_05_pk`,`ul`.`ac_075_pk` AS `ac_075_pk`,`ul`.`ac_1_pk` AS `ac_1_pk`,`ul`.`ac_2_pk` AS `ac_2_pk`,`ul`.`ac_mobil` AS `ac_mobil`,`ul`.`ac_kulkas` AS `ac_kulkas`,`ul`.`ac_lainnya` AS `ac_lainnya`,`ul`.`r22` AS `r22`,`ul`.`r134a` AS `r134a`,`ul`.`r32` AS `r32`,`ul`.`r410a` AS `r410a`,`ul`.`r404a` AS `r404a`,`ul`.`r407a` AS `r407a`,`ul`.`r123` AS `r123`,`ul`.`hidrokarbon` AS `hidrokarbon`,`ul`.`amonia` AS `amonia`,`ul`.`co2` AS `co2`,`ul`.`ac_05_2` AS `ac_05_2`,`ul`.`ac_3_10` AS `ac_3_10`,`ul`.`ac_10` AS `ac_10`,`ul`.`ac_cold` AS `ac_cold`,`ul`.`r290` AS `r290`,`ul`.`r600a` AS `r600a`,`u`.`id_kota` AS `idkota`,`k`.`id_propinsi` AS `id_propinsi` from ((`bengkel_laporan` `ul` join `bengkel` `u` on((`u`.`id` = `ul`.`id_bengkel`))) join `kota` `k` on((`k`.`id` = `u`.`id_kota`)));

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
