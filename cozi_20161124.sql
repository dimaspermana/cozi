ALTER TABLE `besar` ADD `lokasi` VARCHAR( 200 ) NULL AFTER `id_kota` ;

ALTER TABLE `institusi` ADD `lokasi` VARCHAR( 200 ) NULL AFTER `id_kota` ;

ALTER TABLE `supplier` ADD `lokasi` VARCHAR( 200 ) NULL AFTER `id_kota` ;

ALTER TABLE `ukm` ADD `lokasi` VARCHAR( 200 ) NULL AFTER `id_kota` ;

ALTER TABLE `institusi_laporan` ADD `peserta_lulus` INT NULL AFTER `peserta` ;

ALTER TABLE `refrigerasi` ADD `lokasi` VARCHAR( 200 ) NULL AFTER `produk` ;

ALTER TABLE `ac` ADD `lokasi` VARCHAR( 200 ) NOT NULL AFTER `produk` ;
